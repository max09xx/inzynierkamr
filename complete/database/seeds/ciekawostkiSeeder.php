<?php

use Illuminate\Database\Seeder;

class ciekawostkiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $ciekawostka = ["W skład terytorium Japonii wchodzi ponad 6 800 wysp.",
            "W języku japońskim “przepraszam” można wyrazić 20 różnymi słowami, a wybór odpowiedniego w danym momencie uzależniony jest od sytuacji; w zależności od tego, w jaki sposób jest się związanym z daną osobą, można jej wyrazić miłość aż w 5 różnych słowach.",
            "Czwórka to japoński odpowiednik pechowej u nas trzynastki. W japońskiej mitologii cyfra cztery symbolizuje śmierć, więc mieszkańcy Kraju Kwitnącej Wiśni starają się unikać jej jak ognia, i to do tego stopnia, że nawet pomijają ją na przyciskach w windzie, bardzo rzadko też spotyka się produkty pakowane po cztery.",
            "W samym Tokio jest więcej małych zwierząt, takich jak króliki czy szynszyle, niż małych dzieci.",
            "Japonia jest jednym z najczystszych krajów na świecie.",
            "W Japonii zużywa się więcej papieru do drukowania mang niż do produkcji papieru toaletowego.",
            "Japonię co roku nawiedza ok. 1500 trzęsień ziemii.",
            "Japończycy są znani ze swojej punktualności – ich pociągi spóźniają się tam średnio o 18 sekund.",
            "W godzinach szczytu, zwłaszcza w dużych japońskich miastach, pociągi stają się tak zatłoczone, że część podróżnych wręcz się do nich nie mieści. W takich wypadkach do akcji wkraczają specjalnie wyszkoleni pracownicy kolei, których zadaniem jest, kolokwialnie mówiąc, upchanie pasażerów w pociągu tak, by zmieściło się ich tam jak najwięcej, a drzwi pociągu mogły się bezpiecznie domknąć.",
            "Wskaźnik urodzin jest w Japonii tak niski, że w kraju tym sprzedaje się więcej pieluch dla osób dorosłych niż pieluch dziecięcych.",
            "Przechodnie w maseczkach higienicznych na ulicach japońskich miast to tamtejsza codzienność. Japończycy to naród bardzo wyczulony na punkcie higieny osobistej i zdrowia.",
            "W Japonii panują bardzo surowe zasady regulujące możliwość posiadania broni palnej, dlatego do zabójstw z użyciem broni dochodzi tam średnio zaledwie 2 razy w roku.",
            "W Japonii pałeczkami je się praktycznie wszystko. Podczas wizyty w tym kraju należy wziąć jednak pod uwagę fakt, że nieodpowiednie ich użycie, często wynikające z czystej niewiedzy, może zostać odebrane jako przejaw braku kultury i szacunku.",
            "W Japonii żyje ponad 50 000 osób, które przekroczyły setny rok życia.",
            "Zjawisko hikikomori po raz pierwszy zaobserwowano w Japonii w latach 90tych ubiegłego wieku. Jest to przejaw silnej aspołeczności, charakteryzujący się chęcią odizolowania od otoczenia i przebywaniem przez długie miesiące we własnym pokoju, często towarzyszy mu także uzależnienie od internetu.",
            "W Japonii większość telefonów jest wodoodpornych, ponieważ bardzo często używane są pod prysznicem lub w wannie.",
            "Kocie kawiarnie to trend, który dopiero dociera do Europy, a w Azji jest już na porządku dziennym. W kocich kawiarniach nie tylko pija się dobrą kawę, ale przede wszystkim można tam miło spędzić czas w towarzystwie wszechobecnych kociaków.",
            "W Japonii rocznie zużywa się blisko 24 miliardy par pałeczek do jedzenia.",
            "W japońskich szkołach nie ma sprzątaczek. Uczniowie, którzy po lekcjach nie biorą udziału w zajęciach dodatkowych, przez około 2 godziny sprzątają cały budynek. Taka praktyka ma związek z japońskim systemem edukacji, który w dużej mierze skupia się na nauczaniu kultury i dobrego wychowania oraz przygotowaniu młodzieży do samodzielności w dorosłym życiu.",
            "W przeciwieństwie do zachodniego przesądu, w Japonii czarne koty uznawane są za przynoszące szczęście.",
            "Na każdym etapie nauczania japońscy uczniowie noszą inne mundurki. W szkole podstawowej są to najczęściej granatowe swetry i koszule, do których dziewczynki noszą długie do kostek spódnice. W gimnazjach nosi się białe koszulki z narzutką, do których chłopcy zakładają czarne marynarki, a dziewczynki granatowe spódniczki. Uczniowie liceum zakładają białe koszule i marynarki oraz spódniczki przed kolano dla dziewczyn.",
            "Kwadratowy kształt arbuza został wymyślony w Japonii po to, aby owoce były łatwiejsze w transporcie i zajmowały mniej miejsca. Arbuzy przyjmują kwadratowy kształt od pudeł, w których są hodowane.",
            "Lisy cieszą się w Japonii wielkim szacunkiem. Według lokalnej mitologii kitsune, czyli lisy, są symbolem mądrości, posiadają także magiczne umiejętności i zdolność przybierania ludzkiej postaci. Im starszy kitsune, tym większą posiada mądrość, a im większa jest jego mądrość, tym więcej ma ogonów – może zdobyć ich najwięcej dziewięć. Megitsune, czyli lisie matki, w japońskim folklorze czczone były z kolei jako dawczynie życia.",
            "Pracownicy japońskich korporacji nie są karani za sen w pracy. Wręcz przeciwnie, taka drzemka uznawana jest za skutek wytężonego i sumiennego wykonywania swoich obowiązków zawodowych.",
            "Pomimo tego, że Japończycy dobrze się odżywiają, prowadzą zdrowy tryb życia i dożywają sędziwego wieku, Japonia ma największy na świecie wskaźnik anoreksji.",
            "Najczęstszą bezpośrednią przyczyną śmierci japońskich mężczyzn w wieku od 20 do 44 lat jest samobójstwo.",
            "W Japonii większość ulic nie ma żadnej nazwy, dlatego turystom bardzo łatwo jest się tam zgubić, zwłaszcza gdy nie posiadają przy sobie mapy. Na szczęście poproszeni o wskazanie drogi Japończycy są tak uprzejmi, że nierzadko sami zaprowadzają pytającego prosto do celu.",
            "Japończycy mogą poszczycić się najdłuższą na świecie średnią długością życia, która w tym kraju wynosi blisko 84 lata. Niestety, wskaźnik ten jest znacząco zaniżany przez wysoką liczbę samobójstw.",
            "W Japonii zostawianie napiwku kelnerowi lub barmanowi jest postrzegane jako nietakt.",
            "Wbrew panującym stereotypom, Japończycy wcale nie spożywają tak dużo ryżu. Jedzą go głównie na śniadania, w daniu popularnie zwanym bento, które składa się głównie z ryżu i mięsa lub ryb oraz gotowanych warzyw. Danie to często serwowane jest w szkolnych stołówkach ze względu na łatwość i szybkość przygotowania. Co ciekawe, Japończycy jedzą bardzo dużo jajek i to pod wieloma postaciami , poczynając od znanych i u nas omletów, poprzez tradycyjne japońskie roladki, na zupie jajecznej kończąc."
            ];

            foreach ($ciekawostka as $ciekawostki) {
                $ciekawostka = new \App\ciekawatabela();
                $ciekawostka->tresc = $ciekawostki;
                $ciekawostka->save();
            }

    }
}
