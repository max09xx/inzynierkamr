<?php

use Illuminate\Database\Seeder;

class testtabSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $words = ["watashi", "arigatou", "ohayou",
            "konnichiwa", "konbanwa", "sayonara",
            "sumimasen", "hai", "gomennasai", "wakarimasen",
            "hito", "otoko", "onna", "keisatsukan", "kyoushi",
            "ashi", "atama", "kao", "kazoku", "bakemono", "ani",
            "ane", "neko", "buta", "tora", "usagi", "niwatori",
            "sakana", "hebi", "kusa", "hana", "sakura", "suika",
            "tabemono", "hirugohan", "sarada", "pan", "sandoitchi",
            "mizu", "shio", "jikan", "tsuki", "hoshi", "tenki",
            "ame", "yuki", "sora", "hoteru", "fuku", "kimono"];

        $tlumaczenia = ["Ja", "Dziękuję", "Cześć", "Dzień Dobry",
            "Dobry Wieczór", "Do Widzenia", "Przepraszam", "Tak",
            "Bardzo Przepraszam", "Nie Rozumiem", "Osoba", "Mężczyzna",
            "Kobieta", "Policjant", "Nauczyciel Uczelniany", "Noga/Stopa",
            "Głowa", "Twarz", "Rodzina", "Potwór", "Starszy Brat", "Starsza Siostra",
            "Kot", "Świnia", "Tygrys", "Królik", "Kurczak", "Ryba", "Wąż",
            "Trawa", "Kwiat", "Kwitnąca Wiśnia", "Arbuz", "Jedzenie",
            "Lunch", "Sałatka", "Chleb", "Kanapka", "Woda", "Sól",
            "Czas", "Księżyc", "Gwiazda", "Pogoda", "Deszcz", "Śnieg",
            "Niebo", "Hotel", "Ubrania", "Kimono"];

        for($i=0; $i<count($words); $i++){
            $w = new \App\testTableModel();
            $w ->word = $words[$i];
            $w->tlumaczenie = $tlumaczenia[$i];
            $w->save();
        }
    }
}
