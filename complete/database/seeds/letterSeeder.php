<?php

use Illuminate\Database\Seeder;

class letterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $name = ["a.JPG","i.JPG","u.JPG","e.JPG","o.JPG","ka.JPG","ki.JPG","ku.JPG","ke.JPG","ko.JPG",
            "sa.JPG","shi.JPG","su.JPG","se.JPG","so.JPG","ta.JPG","chi.JPG","tsu.JPG","te.JPG","to.JPG",
            "na.JPG","ni.JPG","nu.JPG","ne.JPG","no.JPG","ha.JPG","hi.JPG","fu.JPG","he.JPG","ho.JPG",
            "ma.JPG","mi.JPG","mu.JPG","me.JPG","mo.JPG","ya.JPG","yu.JPG","yo.JPG","ra.JPG","ri.JPG",
            "ru.JPG","re.JPG","ro.JPG","wa.JPG","n.JPG"];

        foreach ($name as $names) {
            $name = new \App\letterModel();
            $name->name = $names;
            $name->save();
        }
    }
}
