<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ciekawostkiSeeder::class);
        $this->call(testtabSeeder::class);
        $this->call(letterSeeder::class);
        $this->call(wordletterSeeder::class);
    }
}
