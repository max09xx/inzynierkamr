<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class wordLetterModel extends Model
{
    public $table = 'wordletter';

    public $fillable = ['id_word', 'id_letter', 'position'];

    public $timestamps = false;
}
