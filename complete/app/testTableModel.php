<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class testTableModel extends Model
{
    public $table = 'testtable';

    public $fillable = ['id', 'word', 'tlumaczenie'];

    public $timestamps = false;
}
