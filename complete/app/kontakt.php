<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kontakt extends Model
{
    public $table = 'kontakt';

    public $fillable = ['name', 'email', 'subject', 'message'];
}

