<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ciekawatabela extends Model
{
    public $table = 'ciekawatabela';

    public $fillable = ['id', 'tresc'];

    public $timestamps = false;
}
