<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class letterModel extends Model
{
    public $table = 'letter';

    public $fillable = ['id', 'name'];

    public $timestamps = false;
}
