<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\kontakt;
use Illuminate\Support\Facades\Mail;

class kontaktController extends Controller
{

    public function kontakt() {
        return view('contact');
    }

    public function kontaktSave (Request $request) {
            $data = $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required'
        ]);

        kontakt::create($data);


try {
    Mail::send('emails.contactus',
        array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'subject' => $request->get('subject'),
            'user_message' => $request->get('message')
        ), function ($message) use ($request) {
            $message->from('example@gmail.com');
            $message->to('max09xx@gmail.com',
                'Admin')->subject($request->get('subject'));
        });
}
catch(\Exeption $data) {
    return redirect('contact');
}


        return back()->with('success', 'Dziękuję za skontaktowanie się ze mną!');
    }

}
