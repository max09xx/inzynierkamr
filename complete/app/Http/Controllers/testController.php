<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class testController extends Controller
{

    public function testowanie() {

        $testSlowa = DB::table('testtable')->orderByRaw('RAND()')->take(5)->get();

        return view('test', [
            'slowa' => $testSlowa
        ]);
    }

    public static function getLetterWord($word_id) {

        $letter = DB::table('wordletter')
            ->join('letter', 'wordletter.id_letter', '=', 'letter.id')
            ->join('testtable', 'wordletter.id_word' , '=', 'testtable.id')
            ->select('letter.name', 'testtable.word', 'wordletter.position', 'letter.id')
            ->where('wordletter.id_word', '=', $word_id)
            ->orderByRaw('RAND()')
            ->get();

        return response()->json($letter->toArray());

    }

    public static function getAnswer(Request $request) {

        $testParts = [];
        $questions = $request->get('question');
        $answer = $request->get('answer');

        $testSlowaIds = [];

        foreach($questions as $question) {
            $testParts[] = DB::table('wordletter')
                ->join('letter', 'wordletter.id_letter', '=', 'letter.id')
                ->join('testtable', 'wordletter.id_word', '=', 'testtable.id')
                ->select('letter.name', 'testtable.word', 'testtable.tlumaczenie', 'wordletter.position', 'wordletter.id_letter', 'testtable.id')
                ->where('wordletter.id_word', '=', $question)
                ->orderBy('wordletter.position', 'ASC')
                ->get();
        }

        $score = [
            'max' => 0,
            'current' => 0,
            'errors' => []
        ];

        foreach($testParts as $testPart) {
            foreach ($testPart as $correctWordLetter) {
                if(isset($answer[$correctWordLetter->id][($correctWordLetter->position-1)])) {
                    if ($correctWordLetter->id_letter == $answer[$correctWordLetter->id][($correctWordLetter->position - 1)]) {
                        $score['current']++;
                    } else {
                        $score['errors'][$correctWordLetter->id] = true;
                    }
                }
                else {
                    $score['errors'][$correctWordLetter->id] = true;
                }

                $score['max']++;

                if(!in_array($correctWordLetter->id, $testSlowaIds)) {
                    $testSlowaIds[] = $correctWordLetter->id;
                }
            }
        }

        $testSlowa = DB::table('testtable')->whereIn('id', $testSlowaIds)->get();

        return view ('test',[
            'answer' => $answer,
            'slowa' => $testSlowa,
            'score' => $score
        ]);
    }
}
