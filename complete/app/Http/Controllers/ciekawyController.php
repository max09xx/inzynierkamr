<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;


class ciekawyController extends Controller
{
    public function ciekawostki() {
        return view('home');
    }

    public static function getCiekawostki() {

        $ciekawostki = DB::table('ciekawatabela')->orderByRaw('RAND()')->take(1)->get();

        return $ciekawostki[0]->tresc;
    }
}
