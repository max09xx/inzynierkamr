<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/kanji', function () {
    return view('kanji');
});

Route::get('/test', 'testController@testowanie')->name ('testStr');

Route::get('check-answer}', 'testController@getAnswer')->name('checkAnswer');

Route::get('/about', function () {
    return view('about');
});

Route::get('/pah', function () {
    return view('pah');
});

Route::get('/pam', function () {
    return view('pam');
});

Route::get('/lekcja1', function () {
    return view('lekcja1');
});

Route::get('/klekcja1', function () {
    return view('klekcja1');
});

Route::get('/klekcja2', function () {
    return view('klekcja2');
});

Route::get('/lekcja2', function () {
    return view('lekcja2');
});

Route::get('/klekcja3', function () {
    return view('klekcja3');
});

Route::get('/lekcja3', function () {
    return view('lekcja3');
});

Route::get('/klekcja4', function () {
    return view('klekcja4');
});

Route::get('/lekcja4', function () {
    return view('lekcja4');
});

Route::get('/klekcja5', function () {
    return view('klekcja5');
});

Route::get('/lekcja5', function () {
    return view('lekcja5');
});

Route::get('/klekcja6', function () {
    return view('klekcja6');
});

Route::get('/lekcja6', function () {
    return view('lekcja6');
});

Route::get('/klekcja7', function () {
    return view('klekcja7');
});

Route::get('/lekcja7', function () {
    return view('lekcja7');
});

Route::get('/klekcja8', function () {
    return view('klekcja8');
});

Route::get('/lekcja8', function () {
    return view('lekcja8');
});

Route::get('/klekcja9', function () {
    return view('klekcja9');
});

Route::get('/lekcja9', function () {
    return view('lekcja9');
});

Route::get('/klekcja10', function () {
    return view('klekcja10');
});

Route::get('/lekcja10', function () {
    return view('lekcja10');
});

Route::get('kontakt',
    'kontaktController@kontakt');

Route::post('kontakt',
    ['as'=>'kontakt.store','uses'=>'kontaktController@kontaktSave']);


Route::get('letter/{word_id}',
    'testController@getLetterWord')->name('litery');
