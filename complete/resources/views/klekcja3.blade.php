@extends('layouts.lekcje')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kra.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
    <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ra">RA</button></p>
    </div>
    <div class="modal fade" id="ra" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA RA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<path class="stm" d="M41.3,37c6.8,0.1,13.6,0.1,20.3,0.2"/>
                        <path class="sts" d="M38.2,45.8c13.3,0.2,22.9,0.3,25.6,0.1c0.2,0,0.6-0.1,0.9,0.2c1.3,1.2-1.4,6.8-4.5,10.7
		c-2.1,2.6-4.3,4.3-5.8,5.3c-3.2,2.2-6.3,3.2-8.3,3.7"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    @endsection

@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kri.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ri">RI</button></p>
    </div>
    <div class="modal fade" id="ri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA RI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<path class="stm" d="M45,34.8c-0.1,5.4-0.1,10.9-0.2,16.3"/>
                        <path class="sts" d="M61.6,33.3c0,5.1,0.1,9.5,0.1,13.5c0,1.5,0.1,3.8-0.8,6.4c-0.7,2-1.7,3.6-2.2,4.3c-0.7,1-1.5,2.3-3,3.7
		c-0.9,0.8-2.4,1.9-4.5,2.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kru.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ru">RU</button></p>
    </div>
    <div class="modal fade" id="ru" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA RU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<path class="stm" d="M45.2,35c0,1,0,2.2,0,3.7c0,3.5,0,4.6,0,5.9c-0.1,1.8-0.1,2.7-0.3,3.7c-0.3,1.5-0.8,2.5-1.5,4.1
		c-0.4,0.9-1.4,2.9-3,5.3c-0.9,1.4-1.8,2.5-2.5,3.3"/>
                        <path class="std" d="M54.8,33.7c0,1.3-0.4,20.8-0.2,23.3c0,0.3,0.1,0.9,0.5,1.2c0.7,0.5,2,0,2.6-0.3c3.9-1.8,6.3-4.1,6.3-4.1
		c1.7-1.6,3.8-3.6,6-6.3"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kre.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#re">RE</button></p>
    </div>
    <div class="modal fade" id="re" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA RE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<path class="stm" d="M44.3,31.3c-0.4,13.2-0.6,22.9-0.5,24.9c0,0.3,0.1,1,0.6,1.4c0.7,0.6,1.8,0.5,3.3,0.1c3.5-0.9,5.8-2,5.8-2
		c0.5-0.3,2.5-1.2,4.9-2.9c0.7-0.5,2.9-2,5.4-4.4c1.4-1.3,3-3,4.7-5.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kro.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ro">RO</button></p>
    </div>
    <div class="modal fade" id="ro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA RO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M42.1,41.8c0,7.4-0.1,14.9-0.1,22.3"/>
                        <path class="sts" d="M41.7,42c7.2,0,14.4,0,21.7,0"/>
                        <path class="std" d="M63.2,41.5c0,7.3,0.1,14.5,0.1,21.8"/>
                        <path class="stl" d="M42,60.6c7.1,0,14.2,0.1,21.2,0.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
