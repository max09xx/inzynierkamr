@extends('layouts.lekcje')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ta.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ta">TA</button></p>
    </div>
    <div class="modal fade" id="ta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA TA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M49.8,39.8c-2.6,6.9-5.2,13.7-7.8,20.6"/>
                        <path class="sts" d="M42.6,45.5c3.6-0.3,7.3-0.7,10.9-1"/>
                        <path class="std" d="M52.9,50.7c0.6-0.3,1.3-0.7,2.3-0.9c1.9-0.5,3.5-0.3,4.4-0.2"/>
                        <path class="stl" d="M52.3,57.5c0.5,0.5,1.2,1,2.1,1.5c2.4,1.2,4.6,0.8,5.5,0.6"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/chi.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#chi">CHI</button></p>
    </div>
    <div class="modal fade" id="chi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA CHI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M41.9,43.4c2.5-0.3,5.3-0.7,8.2-1.4c1.3-0.3,2.6-0.6,3.8-1"/>
                        <path class="sts" d="M48.1,37.5c-0.3,4.6-1.2,8.3-2,10.8c-0.5,1.6-1.3,3.9-0.8,4.2c0.5,0.3,1.7-2.2,4.7-3.3c0.9-0.3,3.5-1.1,5.8,0.3
	c0.3,0.2,2.5,1.6,2.4,3.7c0,1.7-1.5,2.8-2.5,3.6c-2.3,1.8-5,2-6.3,2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/tsu.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tsu">TSU</button></p>
    </div>
    <div class="modal fade" id="tsu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA TSU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M40.7,45.6c4.1-1,6.3-1.6,8.1-2c2.8-0.7,4.4-1.1,6.7-0.9c1.7,0.1,3.9,0.3,5,1.9c0.9,1.3,0.6,2.9,0.4,3.8
	c-0.1,0.7-0.5,2.5-2.1,4c-2.5,2.5-6,2.1-6.4,2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/te.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#te">TE</button></p>
    </div>
    <div class="modal fade" id="te" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA TE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M42,42.8c5.1-0.8,9.2-1.5,12.2-2c0.7-0.1,4.2-0.8,4.3-0.4c0.1,0.3-2.1,0.6-4.3,2.4c-0.3,0.3-2,1.6-3.3,4
	c-0.7,1.3-1.3,2.6-1.3,4.3c0,2.1,1,3.6,1.3,4.2c1.6,2.4,3.9,3.2,4.7,3.5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/to.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#to">TO</button></p>
    </div>
    <div class="modal fade" id="to" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA TO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M46,39.4c1.5,2.6,3,5.3,4.5,7.9"/>
                        <path class="sts" d="M58.8,42.9c-4.2,2.1-7.4,4.2-9.6,5.8c-1.6,1.2-3,2.3-3.3,4.1c-0.2,1.4,0.2,3.1,1.3,4.1c0.8,0.7,1.7,1,4.9,1.2
	c1.7,0.1,3.9,0.2,6.6,0.2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
