@extends('layouts.lekcje')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ka.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ka">KA</button></p>
    </div>
    <div class="modal fade" id="ka" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA KA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M57,42.8c0.7,0.3,2.1,1,3.3,2.4c1,1.2,1.5,2.4,1.8,3.2"/>
                        <path class="sts" d="M49.2,38.3c-2.1,5.8-4.1,11.6-6.2,17.4"/>
                        <path class="std" d="M40.9,46.1c5.3,0.3,7.4-0.8,9.4-0.9c0.6,0,1-0.2,1.6-0.2c1.9,0.2,0.8,3.3,0.5,4.5c-0.4,1.9-1.1,4.8-3.3,8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    @endsection
@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ki.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ki">KI</button></p>
    </div>
    <div class="modal fade" id="ki" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA KI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M49.6,37.6c2.2,4.9,4.4,9.8,6.7,14.7"/>
                        <path class="sts" d="M45.1,44.8c3.9-1.3,7.9-2.7,11.8-4"/>
                        <path class="std" d="M47.7,49.6c3.6-1.2,7.2-2.3,10.8-3.5"/>
                        <path class="stl" d="M46,55.2c0.6,0.8,1.9,2,3.9,2.9c2.5,1.1,4.7,1,5.8,0.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ku.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ku">KU</button></p>
    </div>
    <div class="modal fade" id="ku" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA KU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M54.1,38c-0.9,0.9-2.4,2.3-4.2,4c-3.9,3.7-4.8,4.3-4.8,5.6c0,1.4,1.3,2.1,4.7,5.3c2,1.9,3.5,3.5,4.6,4.7"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ke.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ke">KE</button></p>
    </div>
    <div class="modal fade" id="ke" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA KE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M43.2,38.2c-0.4,1.9-0.8,4.8-0.9,8.2c0,1.2-0.2,7.2,0.9,7.4c0.3,0,0.7-0.3,1.4-1.8"/>
                        <path class="sts" d="M56,37.5c0.2,2.8,0.3,5.7,0.4,8.8c0.1,2.5,0.1,4.2-0.6,6.2c-0.6,1.7-1.4,2.9-2,3.8"/>
                        <path class="std" d="M49.4,43.7c1.5,0.1,3.3,0.2,5.4-0.1c2.4-0.3,4.4-0.9,5.9-1.4"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ko.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ko">KO</button></p>
    </div>
    <div class="modal fade" id="ko" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA KO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M44.8,40.8c3.5-0.5,6.2-0.3,8.1,0c0.9,0.1,2.6,0.4,2.7,0.8c0,0.4-1.4,0.8-2.3,1"/>
                        <path class="sts" d="M43.2,54.4c0.9,0.9,2.6,2.2,5.2,3c2.9,0.9,5.2,0.6,5.7,0.5c1.7-0.3,3-0.8,3.8-1.3"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
