@extends('layouts.app')

@section ('column')
    <hr>
<table class="table table-striped">

    <tr>
        <th>
            <p>N</p>
        </th>
        <th>
            <p>W</p>
        </th>
        <th>
            <p>R</p>
        </th>
        <th>
            <p>Y</p>
        </th>
        <th>
            <p>M</p>
        </th>
        <th>
            <p>H</p>
        </th>
        <th>
            <p>N</p>
        </th>
        <th>
            <p>T</p>
        </th>
        <th>
            <p>S</p>
        </th>
        <th>
            <p>K</p>
        </th>
        <th>
            <p></p>
        </th>
    </tr>

    <tr>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#n">N</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#wa">WA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ra">RA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ya">YA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ma">MA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ha">HA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#na">NA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ta">TA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sa">SA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ka">KA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#a">A</button></p>
        </th>
    </tr>

    <tr>
        <th>
            <p></p>
        </th>
        <th>
            <p></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ri">RI</button></p>
        </th>
        <th>
            <p></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mi">MI</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#hi">HI</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ni">NI</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#chi">CHI</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#shi">SHI</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ki">KI</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#i">I</button></p>
        </th>
    </tr>

    <tr>
        <th>
            <p></p>
        </th>
        <th>
            <p></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ru">RU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#yu">YU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mu">MU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fu">FU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#nu">NU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tsu">TSU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#su">SU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ku">KU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#u">U</button></p>
        </th>
    </tr>

    <tr>
        <th>
            <p></p>
        </th>
        <th>
            <p></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#re">RE</button></p>
        </th>
        <th>
            <p></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#me">ME</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#he">HE</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ne">NE</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#te">TE</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#se">SE</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ke">KE</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#e">E</button></p>
        </th>
    </tr>

    <tr>
        <th>
            <p></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#wo">WO</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ro">RO</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#yo">YO</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mo">MO</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ho">HO</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#no">NO</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#to">TO</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#so">SO</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ko">KO</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#o">O</button></p>
        </th>
    </tr>

    <!-- N -->

    <div class="modal fade" id="n" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA N</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">

	<path class="stm" d="M39,38.8c2.3,1.9,4.6,3.8,6.8,5.7"/>
                        <path class="sts" d="M38.2,63c2.5,0.2,5.9,0,9.5-1.2c4.3-1.4,7-3.6,9-5.2c1.3-1.1,3.5-2.9,5.7-5.8c1.3-1.8,2.1-3.4,2.7-4.7"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <!-- W -->

    <div class="modal fade" id="wa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA WA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M40,38L40,38c-0.1,3.6-0.1,7.2-0.2,10.8"/>
                        <path class="sts" d="M40,38.4c12.2-0.3,21.2-0.5,23.1-0.5c0.4,0,1.3,0,1.8,0.6c0.6,0.7,0.3,1.8,0.2,2.3c-2.3,9-2,8.6-2.7,10
		c-1.2,2.4-2.6,5.4-5.7,8.1c-1.8,1.5-4.1,3-5.3,3.8c-1.3,0.8-2.4,1.4-3.3,1.9"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="wo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA WO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<path class="stm" d="M37.8,40.1c11.6-0.2,20.1-0.3,22.7-0.5c0.6,0,2.2-0.2,3.1,0.8c0.5,0.5,0.6,1.2,0.7,1.8
		c0.3,4.1-2.7,9.3-2.7,9.3c-2.8,4.8-4.2,7.3-6.4,9.3c-4.3,4-9.1,5.5-11.8,6.1"/>
                        <path class="sts" d="M39.2,49.2c7.9-0.1,15.8-0.2,23.6-0.2"/>
	</svg>
                </div>
            </div>
        </div>
    </div>

    <!-- R -->

    <div class="modal fade" id="ra" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA RA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<path class="stm" d="M41.3,37c6.8,0.1,13.6,0.1,20.3,0.2"/>
                        <path class="sts" d="M38.2,45.8c13.3,0.2,22.9,0.3,25.6,0.1c0.2,0,0.6-0.1,0.9,0.2c1.3,1.2-1.4,6.8-4.5,10.7
		c-2.1,2.6-4.3,4.3-5.8,5.3c-3.2,2.2-6.3,3.2-8.3,3.7"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="re" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA RE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<path class="stm" d="M44.3,31.3c-0.4,13.2-0.6,22.9-0.5,24.9c0,0.3,0.1,1,0.6,1.4c0.7,0.6,1.8,0.5,3.3,0.1c3.5-0.9,5.8-2,5.8-2
		c0.5-0.3,2.5-1.2,4.9-2.9c0.7-0.5,2.9-2,5.4-4.4c1.4-1.3,3-3,4.7-5.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA RI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<path class="stm" d="M45,34.8c-0.1,5.4-0.1,10.9-0.2,16.3"/>
                        <path class="sts" d="M61.6,33.3c0,5.1,0.1,9.5,0.1,13.5c0,1.5,0.1,3.8-0.8,6.4c-0.7,2-1.7,3.6-2.2,4.3c-0.7,1-1.5,2.3-3,3.7
		c-0.9,0.8-2.4,1.9-4.5,2.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ru" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA RU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<path class="stm" d="M45.2,35c0,1,0,2.2,0,3.7c0,3.5,0,4.6,0,5.9c-0.1,1.8-0.1,2.7-0.3,3.7c-0.3,1.5-0.8,2.5-1.5,4.1
		c-0.4,0.9-1.4,2.9-3,5.3c-0.9,1.4-1.8,2.5-2.5,3.3"/>
                        <path class="sts" d="M54.8,33.7c0,1.3-0.4,20.8-0.2,23.3c0,0.3,0.1,0.9,0.5,1.2c0.7,0.5,2,0,2.6-0.3c3.9-1.8,6.3-4.1,6.3-4.1
		c1.7-1.6,3.8-3.6,6-6.3"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA RO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M42.1,41.8c0,7.4-0.1,14.9-0.1,22.3"/>
                        <path class="sts" d="M41.7,42c7.2,0,14.4,0,21.7,0"/>
                        <path class="std" d="M63.2,41.5c0,7.3,0.1,14.5,0.1,21.8"/>
                        <path class="stdd" d="M42,60.6c7.1,0,14.2,0.1,21.2,0.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <!-- Y -->

    <div class="modal fade" id="ya" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA YA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M43.2,34.2c3.7,10.4,7.4,20.8,11.2,31.2"/>
                        <path class="sts" d="M35.2,47.8c15.1-3.8,25.7-6.5,27.9-7.3c0.2-0.1,1.1-0.4,1.5-0.1c0.8,0.6,0.1,2.9-0.1,3.7
		C64,45.7,62.9,48.3,60,51"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="yu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA YU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M42.5,39.9c8.7,0,15,0,16.1,0c0.2,0,0.8,0,1.1,0.4c0.3,0.3,0.3,0.8,0.3,1c-0.2,2.8-0.4,12.9-0.4,14.7
		c0,0.5,0,1.4,0,2.5"/>
                        <path class="sts" d="M36.9,58.3c11.3,0,22.5,0,33.8-0.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="yo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA YO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M38.3,40.7c7.3,0.1,14.5,0.1,21.8,0.2"/>
                        <path class="sts" d="M60,40.4c-0.1,9-0.2,17.9-0.3,26.9"/>
                        <path class="std" d="M39.2,51.8c6.9,0,13.8-0.1,20.7-0.1"/>
                        <path class="stdd" d="M38.3,63.8c7.2-0.1,14.3-0.1,21.5-0.2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <!-- M -->

    <div class="modal fade" id="ma" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA MA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M37.2,41.8C50,41.5,59.4,41.2,62.7,41c0.4,0,1.4-0.1,1.8,0.5c0.5,0.8-1,2.2-3.9,5.6c-3.5,4.1-4.1,5.2-7.4,8.6
		c-0.7,0.7-1.3,1.3-1.7,1.7"/>
                        <path class="sts" d="M44.5,51.5c4.4,3.9,8.9,7.9,13.3,11.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA MI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M44,39.5c2.5,0,5.6,0.3,9,1c3.2,0.7,5.9,1.6,8.2,2.5"/>
                        <path class="sts" d="M44.3,49.5c2.6,0.1,5.9,0.5,9.5,1.5c3.1,0.9,5.7,2,7.8,3"/>
                        <path class="std" d="M42.7,59.8c3.1,0.2,7,0.7,11.3,2c3.8,1.1,7,2.5,9.5,3.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA MU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M51.6,35.3c-1.4,4.7-3,9.4-4.8,14.3c-1.3,3.8-2.7,7.5-4.1,11.1"/>
                        <path class="sts" d="M35.1,60.7c10-0.3,20.1-0.6,30.1-0.9"/>
                        <path class="std" d="M58.6,51.3c3,3.9,6,7.8,9,11.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="me" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA ME</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M44.1,44.3c5.4,3.4,10.8,6.8,16.3,10.3"/>
                        <path class="sts" d="M58.4,35.4c-0.7,3.1-2,7.6-4.6,12.5c-0.7,1.3-1.8,3.3-3.4,5.4c-1.9,2.4-5.4,6.3-11.6,9.5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA MO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M39.3,41.3c8.2,0.1,16.4,0.1,24.7,0.2"/>
                        <path class="sts" d="M36,52.3c10.8,0,21.7,0,32.5,0"/>
                        <path class="std" d="M50.8,41.4c-0.1,11.6-0.2,19.9-0.3,21.3c0,0.2-0.1,0.9,0.3,1.5c0.4,0.5,1,0.7,1.3,0.8
		c1.3,0.3,6.9,0.3,14.5-0.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <!-- H -->

    <div class="modal fade" id="ha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA HA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M44,43.3c-0.6,2.5-1.6,5.4-3.2,8.7c-1.7,3.5-3.5,6.2-5.2,8.3"/>
                        <path class="sts" d="M57.2,41.3c1.6,2.1,3.4,4.7,5,7.7c2.3,4.2,3.8,8.2,4.8,11.5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="hi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA HI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M43.3,39.8c-0.1,13.4-0.2,23.2-0.3,24.3c0,0.2,0,0.8,0.3,1.3c0,0,0.3,0.5,0.9,0.9c0.9,0.6,9.2,0.7,20.6,0.3"/>
                        <path class="sts" d="M43.5,51c3.5-0.4,7.2-1,11.3-1.8c3.3-0.7,6.4-1.6,9.3-2.4"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="fu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA FU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M38.8,41.1c13.4,0,22.4-0.1,23.9-0.3c0.2,0,0.9-0.1,1.4,0.3c0.5,0.5,0.3,1.4,0.3,1.8c-1,5.6-0.6,5.1-1.1,6.5
		c-0.2,0.5-1,2.5-2.6,4.7c-1.4,1.9-2.8,3.1-4.2,4.3c-0.9,0.8-2.7,2.2-5.2,3.6c-1.8,1-4.4,2.3-7.7,3.2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="he" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA HE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M38.6,57c5.7-5.5,8.5-8.6,9.7-10.5c0.3-0.4,1.1-1.6,2.1-1.6c1.1,0,2,1.2,2.3,1.6c1.4,1.9,6.2,6.4,16.3,15.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ho" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA HO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M35.8,45.9c10.6,0,21.2,0,31.8,0"/>
                        <path class="sts" d="M51.3,35.9c0,11.1,0.1,22.1,0.1,33.2"/>
                        <path class="std" d="M43.1,52.8c-2.1,3.6-4.2,7.1-6.3,10.7"/>
                        <path class="stdd" d="M59.3,52.8c2.8,3.5,5.7,6.9,8.5,10.4"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <!-- N -->

    <div class="modal fade" id="na" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA NA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M36.3,44.5c10.9,0.1,21.8,0.2,32.7,0.3"/>
                        <path class="sts" d="M52.7,33.8c0,6.4,0.1,11.4,0.2,15.8c0.1,2.1,0.1,4.7-1.2,7.5c-1.2,2.6-3.2,4.4-4,5.2C46.3,63.6,45,64.5,44,65"
                        />
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ni" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA NI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M40.6,44.4c7.5,0,15,0.1,22.5,0.1"/>
                        <path class="sts" d="M36.4,61.5c10.8,0,21.6,0.1,32.4,0.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="nu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA NU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M40.3,40.6c10.6,0,18-0.1,19.5-0.3c0.2,0,1-0.2,1.4,0.3c0.3,0.4,0.2,1.1,0.1,1.4c-1,3.6-2.7,6.2-2.7,6.3
		c-1.5,2.4-2.2,3.5-3.3,4.8c-1.5,1.7-2.7,2.8-4.5,4.4c-1.6,1.4-2.7,2.4-4.3,3.5c-1.6,1.1-4,2.5-7.3,3.7"/>
                        <path class="sts" d="M45,48.1c3,1.6,6.5,3.7,10.2,6.4c3.6,2.7,6.6,5.5,8.9,7.9"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ne" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA NE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M52.2,34.8c0,2.7,0.1,5.4,0.1,8.2"/>
                        <path class="sts" d="M39.8,43.3c10.9-0.3,18.8-0.5,20.3-0.6c0.2,0,0.7,0,0.8,0.2c0.2,0.4-0.6,1.3-1.6,2.3c-2.7,2.7-3.6,3.3-3.6,3.3
		c-3.6,2.7-5.4,4-7.7,5.4c-2.5,1.5-5.8,3.4-9.8,5.2"/>
                        <path class="std" d="M52,51.3c0,5.6,0,11.2,0.1,16.8"/>
                        <path class="stdd" d="M59.2,52.8c2.8,1.9,5.7,3.8,8.5,5.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="no" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA NO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M60.3,36.4c-0.8,3.3-2.4,8.2-5.9,13.3c-4.5,6.6-9.9,10.5-13.1,12.4"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <!-- T -->

    <div class="modal fade" id="ta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA TA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M48.2,36.8c-0.7,2.4-1.9,5.5-4,8.8c-1.8,3-3.8,5.2-5.5,6.8"/>
                        <path class="sts" d="M45.8,42.7c6.8,0.1,11.7,0.1,13.3,0.1c0.1,0,0.3,0,0.5,0.1c1.5,0.8-0.2,6.3-0.3,6.6c-0.7,2.1-1.5,3.5-3.1,6.1
		c-1.5,2.5-2.4,3.9-3.9,5.6c-2,2.1-3.8,3.4-5.4,4.5c-1.8,1.2-3.5,2.1-4.8,2.8"/>
                        <path class="std" d="M47,51c4.5,2.8,8.9,5.6,13.4,8.4"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="chi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA CHI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M62,40.8c-2.8,0.9-6.4,1.8-10.7,2.3c-4.2,0.5-7.9,0.4-10.8,0.2"/>
                        <path class="sts" d="M36.2,52c10.4-0.1,20.8-0.1,31.2-0.2"/>
                        <path class="std" d="M53.4,43.2c-0.2,4.4-0.1,8,0,10.4c0.1,2.9,0.3,4.3-0.5,6.3c-0.9,2.1-2.4,3.6-4.1,5.3c-1.6,1.5-3.1,2.6-4.3,3.4
		"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="tsu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA TSU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M40.7,42.3c1.4,2.4,2.9,4.8,4.3,7.2"/>
                        <path class="sts" d="M50.5,39.7c1.3,2.4,2.6,4.9,3.8,7.3"/>
                        <path class="std" d="M67.2,40.3c0.1,2.8-0.1,7.5-2.5,12.5c-0.8,1.7-2.1,4.4-5,7c-2,1.8-4,2.8-6,3.8c-2.4,1.2-4.6,2-6.2,2.5"/>

</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="te" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA TE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M40.9,39.8c6.7,0,13.4-0.1,20.2-0.1"/>
                        <path class="sts" d="M35.3,49.8c10.6-0.1,21.2-0.1,31.8-0.2"/>
                        <path class="std" d="M52.6,49.7c0.1,1,0.1,2.3,0.1,3.8c0,0.9-0.1,2.5-0.5,4.3c-0.2,1.1-0.6,2.9-1.6,4.8c-0.6,1.3-2,3.5-4.5,5.5"/>

</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="to" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA TO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M47.1,34.9C47.1,45,47,55.1,47,65.3"/>
                        <path class="sts" d="M47,47.7c2.2,0.1,5.4,0.5,9,1.9c2.8,1.1,4.9,2.4,6.5,3.6"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <!-- S -->

    <div class="modal fade" id="sa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA SA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M33.8,48c11.8,0,23.5,0.1,35.3,0.1"/>
                        <path class="sts" d="M43.8,38.4c0,6.3,0.1,12.7,0.1,19"/>
                        <path class="std" d="M58.1,37.2C58,38.7,58,40.7,58,43c0,1.7,0.1,2.2,0.1,5.1c0.1,2.9,0.1,4.4,0,5.9c-0.2,2.7-0.2,4-0.8,5.3
		c-0.8,1.9-2,2.9-4.3,4.8c-1.4,1.1-3.5,2.7-6.3,4.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="shi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA SHI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M43.7,39.3c2.4,1.4,4.8,2.9,7.2,4.3"/>
                        <path class="sts" d="M38.8,48.5c2.4,1.6,4.8,3.1,7.2,4.7"/>
                        <path class="std" d="M42.7,67.8c3.2-1.1,5.8-2.3,7.7-3.2c3.3-1.6,5.5-2.6,8.2-4.7c2.4-1.9,4.1-3.7,5.5-5.2c1.7-1.9,3-3.5,4-4.8"/>

</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="su" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA SU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M42.4,40.6c9.2-0.2,16-0.4,17.8-0.3c0.4,0,1.4,0,1.8,0.6c0.3,0.5,0.1,1.1,0,1.5c-1.2,3.6-4.4,6.9-4.4,6.9
		c-2,2.1-3,3.1-4.9,4.9c-1.9,1.8-3.6,3.4-6,5.3c-1.7,1.3-3.9,3-6.6,4.8"/>
                        <path class="sts" d="M55.2,51.8c2.4,1.5,5.1,3.5,7.8,6c2.3,2.1,4.3,4.2,5.9,6.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="se" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA SE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M35.1,51.8c14.1-2.6,24.2-4.4,26.5-5c0.4-0.1,1.9-0.5,2.2,0.1c0.2,0.4-0.2,1-0.3,1.3c-1.5,2.5-6.1,8.8-6.8,9.7
		"/>
                        <path class="sts" d="M46.1,38.3c0.2,14.2,0.3,24.4,0.2,26.1c0,0.3-0.1,1.3,0.5,1.8c0.5,0.4,1.1,0.3,1.3,0.3
		c1.5-0.1,7.6-0.2,16.2-0.3"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="so" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA SO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M36.1,39.8c1.8,2.8,3.5,5.5,5.3,8.3"/>
                        <path class="sts" d="M59.8,38.6c0.1,2.6,0,7.3-2.4,12.4c-1.8,3.9-4.1,6.3-5.6,7.9c-3.7,3.8-7.6,6.1-10.6,7.5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <!-- K -->

    <div class="modal fade" id="ka" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA KA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M36.6,44.8c13.3-0.2,22.7-0.4,25.3-0.8c0.2,0,0.6-0.1,1.1,0.2c1.3,0.8,0.5,3.3,0.4,9.1c0,2.1,0,4-0.6,6.6
		c-0.3,1.3-0.6,2.1-1.2,2.8c-1.2,1.4-2.8,1.8-3.9,2c-1.8,0.4-3.4,0.2-4.4-0.1"/>
                        <path class="sts" d="M50.6,34.8c0.1,5.2-0.4,9.5-0.9,12.5c-0.9,5-1.9,7.2-2.4,8.1c-1.4,2.7-3.1,4.6-4.1,5.8c-1.8,2-3.5,3.4-4.9,4.4
		"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ki" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA KI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M37.5,47.3c8.1-1.1,16.2-2.1,24.3-3.2"/>
                        <path class="sts" d="M34.3,58.7c11.1-1.3,22.2-2.7,33.3-4"/>
                        <path class="std" d="M47.2,37.8c2.6,10.7,5.1,21.4,7.7,32.2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ku" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA KU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M49.9,35.3c-0.2,2-0.8,5.1-2.6,8.4c-2.3,4.2-5.3,6.7-7,8"/>
                        <path class="sts" d="M47.9,42.3c7.5-0.1,12.8-0.2,13.8-0.3c0.1,0,0.4-0.1,0.7,0.1c0.5,0.3,0.7,1.1,0.7,1.7c0.2,3.6-0.7,6.1-0.8,6.1
		c-1.7,4.4-4.1,7.3-4.3,7.5c-4.3,5.2-9.7,7.9-13,9.3"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ke" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA KE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M45.9,36.5c-0.2,2.1-0.8,5.3-2.5,8.8c-1.8,3.9-4.2,6.5-5.8,8.1"/>
                        <path class="sts" d="M43.1,45.9c8.4,0,16.7,0.1,25.1,0.1"/>
                        <path class="std" d="M57.3,46c0.1,2.3,0,5.4-1.3,8.9c-1.1,3.1-2.6,5.1-3.7,6.5c-2.1,2.8-4.4,4.8-6.1,6"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ko" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA KO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M39.8,39.9c7.9,0,15.8,0.1,23.7,0.1"/>
                        <path class="sts" d="M63.4,39.6c0,7,0,14,0,21"/>
                        <path class="std" d="M39.8,57.6c7.9,0,15.8,0,23.7,0"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <!-- NORMAL -->

    <div class="modal fade" id="a" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA A</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M34.4,40.9c16,0,25.6-0.2,27.6-0.5c0.2,0,1-0.2,1.3,0.2c0.4,0.6-1.1,2.2-1.9,3c-1.6,1.6-4.1,4.3-7.9,8"/>
                        <path class="sts" d="M49.3,46.4c0,1.6-0.1,3.9-0.3,6.7c-0.3,3.4-0.5,5.1-1.2,6.8c-0.6,1.5-2.2,4.8-7.3,7.5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="i" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA I</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M61.2,37.8c-3.1,3.9-6.1,6.9-8.5,9c-1.6,1.4-4.2,3.7-8,6c-2.7,1.7-5.2,2.9-7.2,3.7"/>
                        <path class="sts" d="M52.3,47.2c0.1,7.2,0.1,14.3,0.2,21.5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="u" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA U</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M50.8,33.4c0,2.9,0,5.9,0,8.8"/>
                        <path class="sts" d="M38.5,42c0,3.5-0.1,6.9-0.1,10.4"/>
                        <path class="std" d="M38.1,42.1c13.2-0.2,22.9-0.2,24.6-0.2c0.3,0,1,0,1.5,0.5c0.6,0.6,0.6,1.7,0.5,2.2c-0.5,5.1-2.2,8.5-2.2,8.5
		c-0.8,1.6-1.8,3.6-3.7,5.7c-1.5,1.6-2.8,2.5-5.6,4.4c-1.8,1.2-3.4,2.2-4.5,2.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="e" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA E</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M39.8,41c8.4,0.1,16.8,0.1,25.3,0.2"/>
                        <path class="sts" d="M52.6,41.1c0,6.3-0.1,12.6-0.1,18.8"/>
                        <path class="std" d="M37,60.1c10.3,0,20.5,0,30.8,0"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="o" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA O</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M40,43.3c11.2,0.1,22.3,0.1,33.5,0.2"/>
                        <path class="sts" d="M58.7,33.4c0,2,0.6,28.6,0.3,30.2c-0.1,0.6-0.4,1-0.4,1c-0.4,0.6-1,0.8-1.3,0.9c-1.7,0.7-4.7,0.4-5,0.3"/>
                        <path class="std" d="M58.9,43.4c-2,3.4-4.9,7.5-9.1,11.7c-3,2.9-5.9,5.1-8.4,6.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>
</table>
    <hr>
@endsection
