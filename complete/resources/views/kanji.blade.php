@extends('layouts.app')

@section ('column')
<hr>
    <div class="gif">

        <p>
            W lekcjach z Hiragany i Katakany poznaliście litery oraz jak powinno się je pisać.
        </p>

        <p>
            Tu jednak będzie jedynie zawarta teoria choć i ją warto przeczytać.
        </p>

        <p>
            Może po kolei.
        </p>

        <p>
            Kanji są to znaki pochodzenia chińskiego, które wraz ze znakami hiragany, katakany, cyframi arabskimi oraz alfabetem łacińskim stanowią element pisma japońskiego. W prostym tego zdania znaczeniu, jest to połączenie wszystkich aspektów pisma w jedno wielkie skupisko.
        </p>

        <p>
            Trochę o historii Kanji (choć bądźmy szczerzy mało który uczeń lubi czytać o historii pisma więc postaram się to zwęzić jak najbardziej).
        </p>

        <p>
            Kanji są podstawą języka pisanego w Japonii. Zostały do niego dołączone w V i VI i dotarły z Chin, podróżując przez Koreę. Początkowo było to pismo głównie używane w dokumentacjach i tekstach religijnych.
        </p>

        <p>
            Znaki kanji są logogramami, co znaczy że nie reprezentują pozbawionego znaczenia dźwięku, lecz słowa. Pismo logograficzne poza oczywistą trudnością jaką jest nauka i zapamiętanie ich, ma zalety takie jak szybkość czytania i mało zajmujące miejsca pismo.
        </p>

        <p>
            Tyle z teorii, nie było tak źle prawda? Teraz kilka przykładów znaków kanji.
        </p>

    </div>
<hr>
    <div class="gif">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="img/kanji1.jpg" width=350px height=350px alt="Coś poszło nie tak"/>
                </div>
                <div class="col-12 col-md-6">
                    <img src="img/kanji3.jpg" width=350px height=350px alt="Coś poszło nie tak"/>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="img/kanji2.jpg" width=700px height=300px alt="Coś poszło nie tak"/>
                </div>
            </div>
        </div>
    </div>
<hr>

    <div class="gif">

        <p>
            Jak widać na zdjęciach, znaki kanji w części oddają wygląd rzeczy które opisują lub są połączeniem kilku w jedno, tak aby stworzyć kolejne znaczenie.
        </p>

        <p>
            Japończycy często sami mają problem z odczytywaniem znaków kanji. Dlatego często uczęszczają na różnego rodzaju zajęcia oraz egzaminy, dotyczące właśnie znaków kanji.
        </p>

        <p>
            Chcąc się nauczyć jak odczytywać znaki kanji potrzeba wiele wytrwałości w tym co się robi. Choć często mogą się mylić wam znaki, nie poddawajcie się i małymi krokami dotrzecie do celu.
        </p>

        <p>
            To tyle z tej lekcji. Mam nadzieję że nie zanudziłem nikogo i choć w małym stopniu przybliżyłem o co tak na prawdę w tym chodzi.
        </p>

        <h1>
            Dziękuję za uwagę!
        </h1>
    </div>
<hr>
    <p>.</p>

@endsection
