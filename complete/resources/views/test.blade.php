@extends('layouts.test')

@section('slowo')

<h1>Obecny winik (punkt za trafiony znak): {{$score['current'] ?? '0'}}</h1>

@endsection

@section('znaki')

    <form action="{{ route('checkAnswer') }}" method="get">
        <hr>
        @foreach($slowa as $slowo)
            <div class="py-3">
                <h3 class="text-center">{{$slowo->word}} - {{$slowo->tlumaczenie}}</h3>
                <drag word_id="{{$slowo->id}}"></drag>
            </div>
            <input type="hidden" name="question[]" value="{{ $slowo->id }}" />
            <hr>
        @endforeach

            <div class="col-12">
                <button class="btn btn-success">Sprwadź</button>
            </div>

    </form>

@endsection

@section('odpowiedz')
    <form action="{{ route('testStr') }}" method="get">

        <div class="col-12">
            <button class="btn btn-success">Inne słowa</button>
        </div>

    </form>
    <hr>
    <p>.</p>
@endsection
