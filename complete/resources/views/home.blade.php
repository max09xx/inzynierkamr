@extends('layouts.app')

@section('column')

    <div class="gif">
        <h1>
            OPŁATY: BRAK!
        </h1>

        <h2>
            Kto może skorzystać? KAŻDY!
        </h2>
    </div>
<hr>
    <p>
        Czego od Ciebie oczekuje? Oczekuję że dobrze wykorzystasz ten podstawowy kurs, będziesz się przy tym dobrze bawił oraz wesprzesz mnie wysyłając opinię na temat strony przez zakładkę "Kontakt" lub polub moje media społecznościowe.
    </p>

    <p>
        Do rzeczy. Wiem że kurs nie jest bardzo rozbudowany ale właśnie taki był zmiar przy tworzeniu go. Ten kurs ma zapewnić poznanie podstaw alfabetów tego oto języka. Nie pokażę Ci jak budować dokładnie zdania czy jak używać czasów, ale to nic! Ten kurs to jedynie wstępny krok który Cię przybliży do poznania tego pięknego języka i jest darmowy! Czy to nie jest idealne?
    </p>

    <p>
        Odśwież strone a dowiesz się kilku ciekawych rzeczy o Japonii.
    </p>

@endsection

@section('columnri')
    <hr>
<div class="gif">
    <img src="img/japan.jpg" width=400px height=300px alt="Coś poszło nie tak"/>
</div>

    @endsection

@section('columnle')
    <hr>
    <p class="italic">{{ \App\Http\Controllers\ciekawyController::getCiekawostki()}}</p>

@endsection
