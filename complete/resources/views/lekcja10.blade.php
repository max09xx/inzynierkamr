@extends('layouts.lekcje1')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/wa.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#wa">WA</button></p>
    </div>
    <div class="modal fade" id="wa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA WA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">
</style>
                        <path class="stm" d="M42.9,45.6c0.9-0.2,2.3-0.6,3.9-1.3c1.4-0.6,1.8-0.9,2-0.8c0.8,0.8-7,10.3-6.4,10.9c0.3,0.2,1.7-1.1,7.1-5.3
		c2.5-2,3.7-2.8,5.6-3.4c1.5-0.4,3.7-1.1,5.6,0.1c2.3,1.4,2.5,4.3,2.5,4.5c0.1,2-1,3.5-1.6,4.4c-0.9,1.2-1.9,1.9-3,2.6
		c-1.5,1-2.9,1.6-3.9,2"/>
                        <path class="std" d="M47.5,39.3c0,6.5-0.1,13.1-0.1,19.6"/>
	</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

