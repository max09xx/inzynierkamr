@extends('layouts.lekcje3')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ya.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ya">YA</button></p>
    </div>
    <div class="modal fade" id="ya" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA YA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M42.1,41c2.3,6.5,4.6,12.9,6.9,19.4"/>
                        <path class="sts" d="M39.1,48.3c3.3-1.4,6.1-2.5,8.3-3.3c0.7-0.3,2.3-0.8,4.4-1.3c1.9-0.4,3.1-0.6,4.6-0.3c0.8,0.2,3.2,0.8,3.6,2.5
	c0.4,1.4-0.8,2.7-1.1,3.1c-1.3,1.5-3,1.7-3.5,1.8"/>
                        <path class="std" d="M48.4,38.6c0.9,0.9,1.8,1.8,2.8,2.6"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/yu.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#yu">YU</button></p>
    </div>
    <div class="modal fade" id="yu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA YU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M42.1,44.5c0.1,1.5,0.2,3.7,0.3,6.4c0,0,0.1,3.5,0.3,3.5c0.1,0-0.7-3.3,1-5.9c0.8-1.2,1.8-1.8,4-3.1
	c2.3-1.4,3.6-2.1,5.5-2.4c1.1-0.1,3.6-0.5,5.8,1.1c1.8,1.3,2.4,3,2.5,3.5c0.3,0.8,0.7,3-0.5,5.1c-1,1.7-2.6,2.4-3.3,2.6
	c-1.7,0.7-3.3,0.5-4.6,0.4c-1.6-0.2-2.9-0.6-3.8-1"/>
                        <path class="std" d="M52.9,38.3c0.6,2.5,0.9,4.7,1,6.3c0.2,2.5,0,2.1,0.3,6.5c0.1,1.6,0.2,2.4,0,3.8c-0.1,1.1-0.3,2.5-1.3,3.9
	c-0.7,1.1-1.6,1.7-2.1,2.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/yo.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#yo">YO</button></p>
    </div>
    <div class="modal fade" id="yo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA YO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M51.8,39.2c0.1,3.3,0.1,6.3,0.2,8.7c0.1,9.6-0.2,3.5-0.1,5.6c0.1,2.2,0.2,3.2-0.5,4.3c-0.8,1.1-2,1.5-3,1.8
	c-0.8,0.2-3.9,1.1-5-0.5c-0.6-0.8-0.5-2.2,0.1-3c0.9-1.2,2.5-1.2,3.9-1.1c1.2,0,3,0.3,6.9,2.1c1.3,0.6,3.2,1.6,5.3,2.9"/>

                        <path class="std" d="M58.5,43.4c-2.1,0.5-4.3,0.9-6.4,1.4"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

