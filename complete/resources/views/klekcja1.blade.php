@extends('layouts.lekcje')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kn.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
    <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#n">N</button></p>
    </div>
    <div class="modal fade" id="n" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA N</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">

	<path class="stm" d="M39,38.8c2.3,1.9,4.6,3.8,6.8,5.7"/>
    <path class="sts" d="M38.2,63c2.5,0.2,5.9,0,9.5-1.2c4.3-1.4,7-3.6,9-5.2c1.3-1.1,3.5-2.9,5.7-5.8c1.3-1.8,2.1-3.4,2.7-4.7"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kaa.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

            <div class="gif">
    <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#a">A</button></p>
            </div>
    <div class="modal fade" id="a" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA A</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M34.4,40.9c16,0,25.6-0.2,27.6-0.5c0.2,0,1-0.2,1.3,0.2c0.4,0.6-1.1,2.2-1.9,3c-1.6,1.6-4.1,4.3-7.9,8"/>
    <path class="std" d="M49.3,46.4c0,1.6-0.1,3.9-0.3,6.7c-0.3,3.4-0.5,5.1-1.2,6.8c-0.6,1.5-2.2,4.8-7.3,7.5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kii.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

                    <div class="gif">
    <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#i">I</button></p>
                    </div>
    <div class="modal fade" id="i" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA I</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M61.2,37.8c-3.1,3.9-6.1,6.9-8.5,9c-1.6,1.4-4.2,3.7-8,6c-2.7,1.7-5.2,2.9-7.2,3.7"/>
    <path class="std" d="M52.3,47.2c0.1,7.2,0.1,14.3,0.2,21.5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kuu.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

                            <div class="gif">
    <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#u">U</button></p>
                            </div>
    <div class="modal fade" id="u" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA U</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M50.8,33.4c0,2.9,0,5.9,0,8.8"/>
    <path class="sts" d="M38.5,42c0,3.5-0.1,6.9-0.1,10.4"/>
    <path class="std" d="M38.1,42.1c13.2-0.2,22.9-0.2,24.6-0.2c0.3,0,1,0,1.5,0.5c0.6,0.6,0.6,1.7,0.5,2.2c-0.5,5.1-2.2,8.5-2.2,8.5
		c-0.8,1.6-1.8,3.6-3.7,5.7c-1.5,1.6-2.8,2.5-5.6,4.4c-1.8,1.2-3.4,2.2-4.5,2.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kee.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

                                    <div class="gif">
    <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#e">E</button></p>
                                    </div>
    <div class="modal fade" id="e" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA E</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M39.8,41c8.4,0.1,16.8,0.1,25.3,0.2"/>
    <path class="std" d="M52.6,41.1c0,6.3-0.1,12.6-0.1,18.8"/>
    <path class="stl" d="M37,60.1c10.3,0,20.5,0,30.8,0"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table6')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/koo.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

                                            <div class="gif">
    <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#o">O</button></p>
                                            </div>
    <div class="modal fade" id="o" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA O</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M40,43.3c11.2,0.1,22.3,0.1,33.5,0.2"/>
    <path class="std" d="M58.7,33.4c0,2,0.6,28.6,0.3,30.2c-0.1,0.6-0.4,1-0.4,1c-0.4,0.6-1,0.8-1.3,0.9c-1.7,0.7-4.7,0.4-5,0.3"/>
    <path class="stl" d="M58.9,43.4c-2,3.4-4.9,7.5-9.1,11.7c-3,2.9-5.9,5.1-8.4,6.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
