@extends('layouts.lekcje')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kka.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ka">KA</button></p>
    </div>
    <div class="modal fade" id="ka" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA KA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M36.6,44.8c13.3-0.2,22.7-0.4,25.3-0.8c0.2,0,0.6-0.1,1.1,0.2c1.3,0.8,0.5,3.3,0.4,9.1c0,2.1,0,4-0.6,6.6
		c-0.3,1.3-0.6,2.1-1.2,2.8c-1.2,1.4-2.8,1.8-3.9,2c-1.8,0.4-3.4,0.2-4.4-0.1"/>
                        <path class="std" d="M50.6,34.8c0.1,5.2-0.4,9.5-0.9,12.5c-0.9,5-1.9,7.2-2.4,8.1c-1.4,2.7-3.1,4.6-4.1,5.8c-1.8,2-3.5,3.4-4.9,4.4
		"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kki.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ki">KI</button></p>
    </div>
    <div class="modal fade" id="ki" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA KI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M37.5,47.3c8.1-1.1,16.2-2.1,24.3-3.2"/>
                        <path class="sts" d="M34.3,58.7c11.1-1.3,22.2-2.7,33.3-4"/>
                        <path class="std" d="M47.2,37.8c2.6,10.7,5.1,21.4,7.7,32.2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kku.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ku">KU</button></p>
    </div>
    <div class="modal fade" id="ku" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA KU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M49.9,35.3c-0.2,2-0.8,5.1-2.6,8.4c-2.3,4.2-5.3,6.7-7,8"/>
                        <path class="sts" d="M47.9,42.3c7.5-0.1,12.8-0.2,13.8-0.3c0.1,0,0.4-0.1,0.7,0.1c0.5,0.3,0.7,1.1,0.7,1.7c0.2,3.6-0.7,6.1-0.8,6.1
		c-1.7,4.4-4.1,7.3-4.3,7.5c-4.3,5.2-9.7,7.9-13,9.3"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kke.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ke">KE</button></p>
    </div>
    <div class="modal fade" id="ke" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA KE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M45.9,36.5c-0.2,2.1-0.8,5.3-2.5,8.8c-1.8,3.9-4.2,6.5-5.8,8.1"/>
                        <path class="sts" d="M43.1,45.9c8.4,0,16.7,0.1,25.1,0.1"/>
                        <path class="std" d="M57.3,46c0.1,2.3,0,5.4-1.3,8.9c-1.1,3.1-2.6,5.1-3.7,6.5c-2.1,2.8-4.4,4.8-6.1,6"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kko.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ko">KO</button></p>
    </div>
    <div class="modal fade" id="ko" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA KO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M39.8,39.9c7.9,0,15.8,0.1,23.7,0.1"/>
                        <path class="sts" d="M63.4,39.6c0,7,0,14,0,21"/>
                        <path class="std" d="M39.8,57.6c7.9,0,15.8,0,23.7,0"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
