@extends('layouts.lekcje')

@section('table1')

<div class="gif">
    <figure class="figure">
        <img src="/img/screenyHiragana/a.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
    </figure>
</div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#a">A</button></p>
    </div>
<div class="modal fade" id="a" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA A</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">
</style>
                    <path class="stm" d="M43.1,45.1c2.1,0.2,3.9,0.1,5.3,0c0.5,0,2-0.2,3.9-0.6c1.2-0.2,2.2-0.5,3-0.8"/>

                    <path class="sts" d="M49.2,39.2c0,2-0.1,3.7-0.2,4.8c-0.1,1.5-0.1,1.9-0.3,4.4c-0.2,2.8-0.2,3.5-0.2,4.1c0,0.5,0,1.3,0.2,3.4
	c0.1,1.3,0.3,3.2,0.5,5.4"/>

                    <path class="std" d="M53.9,48.3c-0.4,1.1-1,2.3-1.6,3.6c-0.8,1.6-1.5,2.7-2.1,3.5c-1.2,1.8-1.8,2.7-2.7,3.6
	c-0.9,0.8-1.7,1.5-2.9,1.8c-0.5,0.1-2,0.4-3-0.6c-0.9-0.9-0.7-2.3-0.6-2.6c0.2-0.9,0.7-1.5,1.6-2.4c1.1-1.2,2.2-1.9,2.8-2.2
	c1.8-1.1,3.3-1.5,4.4-1.8c1.6-0.4,2.9-0.8,4.6-0.6c0.9,0.1,1.9,0.2,3.1,0.9c0.5,0.3,1.5,0.9,2.1,2.3c0.6,1.2,0.5,2.4,0.4,3
	c-0.1,0.5-0.3,2-1.4,3.3c-0.8,0.9-1.8,1.3-2.3,1.6c-0.7,0.3-1.3,0.4-1.7,0.5"/>
</svg>
            </div>
        </div>
    </div>
</div>

@endsection
@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/i.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#i">I</button></p>
    </div>
    <div class="modal fade" id="i" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA I</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="sts" d="M42.5,45.7c-0.1,2.9,0.3,5.3,0.6,7c0.4,2.2,1,4,2.4,5.3c0.3,0.3,1.1,1.1,1.6,0.8c0.5-0.2,0.5-1.2,0.3-2.6"/>
                        <path class="stm" d="M56.7,45.8c1,1.1,2.1,2.7,3.1,4.8c0.8,1.6,1.3,3.2,1.6,4.4"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/u.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#u">U</button></p>
    </div>
    <div class="modal fade" id="u" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA U</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M49.3,41.1c0.6,0.2,1.5,0.5,2.3,1.1c0.9,0.6,1.6,1.2,2.1,1.7"/>
                        <path class="sts" d="M46.3,50.2c0.5-0.5,1.4-1.4,2.8-2c0.8-0.3,2.2-1,4-0.6c0.4,0.1,2.1,0.5,3.2,1.9c1.3,1.8,0.7,4,0.3,5.6
	c-0.4,1.7-1.1,2.8-2.1,4.4c-0.8,1.2-1.9,3-3.7,4.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/e.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#e">e</button></p>
    </div>
    <div class="modal fade" id="e" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA E</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M47.3,42.8c1.7,0.9,3.4,1.9,5.1,2.8"/>
                        <path class="stdd" d="M43.6,50.9c5.6-2.3,9.8-3.5,10.5-2.4c1.5,2.4-12.6,15-12.8,14.8c-0.2-0.2,6.5-7.4,8.7-6.9
	c3.3,0.6,0.5,5.1,1.7,6.8c0.6,0.9,2.1,1.1,3.3,1c2.4-0.3,5.6-1.5,4.4-1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/o.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#o">O</button></p>
    </div>
    <div class="modal fade" id="o" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA O</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M56.5,45.6c0.6,0.3,1.4,0.8,2.3,1.5c0.7,0.6,1.3,1.2,1.7,1.8"/>
                        <path class="sts" d="M42,49.7c1.4-0.1,3.1-0.3,5.1-0.9c1.6-0.5,2.9-1,4-1.6"/>
                        <path class="std" d="M46.9,43.3C46.7,46.5,47,49.2,47,51c-0.1,3.3,0.5,4.4,0.2,8c-0.2,2.5-0.5,3.6-1.3,4c-1.1,0.6-3.4,0.6-3.9-0.3
	c-0.8-1.5,1.1-4.2,1.4-4.5c1.3-1.3,3.6-2.2,6.2-2.9c2.9-0.8,4.8-1.1,5.3-1.1c2.4-0.1,3.7,0.5,4.7,1.5c1.4,1.5,0.6,4,0.5,4.3
	c-1,3.1-4.6,3.9-4.8,3.9"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table6')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/n.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#nn">N</button></p>
    </div>
    <div class="modal fade" id="nn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA N</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">
</style>
                        <path class="stm" d="M54,38.3C48.2,49.4,43.7,58,43.7,58c0,0,4.5-9.6,7.2-9c0.6,0.1,1.6,0.9,1.8,1.8c0.1,0.5,0,0.7,0,1.6
		c0,0.7-0.1,2,0.2,2.9c0.6,1.6,2.7,2.2,3.3,2.3c2.9,0.8,5.3-0.8,5.5-1c1.9-1.3,2.5-3.2,2.7-3.8"/>
		</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
