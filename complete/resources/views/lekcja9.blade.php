@extends('layouts.lekcje')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ra.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ra">RA</button></p>
    </div>
    <div class="modal fade" id="ra" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA RA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M46.8,45c0.4,3.1,0.2,5.5,0,7.1c-0.1,1-0.4,2.3,0,2.5c0.4,0.2,0.9-0.7,2.1-1.7c1.9-1.5,3.8-1.9,4.4-2
	c0.9-0.2,2.1-0.4,3.6,0c0.6,0.2,2.1,0.6,3,2c0.2,0.3,0.9,1.4,0.7,2.8c-0.2,1.1-0.9,1.8-1.4,2.3c-0.8,0.7-1.6,1.1-2.3,1.3
	c-1.8,0.7-3.5,1.4-5,1.9"/>
                        <path class="std" d="M48.2,38.3c1.1,1.1,2.2,2.1,3.3,3.2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ri.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ri">RI</button></p>
    </div>
    <div class="modal fade" id="ri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA RI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M46,44c-0.5,1.9-0.7,3.5-0.8,4.6c-0.1,2.2,0.3,3.3,0.4,3.6c0.3,0.9,0.8,1.7,1.1,2.1"/>
                        <path class="sts" d="M54.1,43.7c0.7,2,1,3.8,1,5.1c0,0.6,0.4,1.2,0,4.7c-0.3,3.1-0.9,4.4-1,4.9c-0.4,1.4-1.2,3.4-3.4,5.5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ru.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ru">RU</button></p>
    </div>
    <div class="modal fade" id="ru" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA RU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M46.8,41.7c2.6,0,4.6-0.3,6-0.7c1.3-0.3,2.6-0.8,2.8-0.5c0.1,0.2-0.8,0.8-1.8,1.7c-1.4,1.3-1.4,1.8-3,3.8
	c-1.4,1.7-2.7,3-4.6,4.7c-1.3,1.2-2.5,2.2-2.4,2.3c0.1,0.1,1.4-1.3,3.7-2.3c1.9-0.8,3.6-0.8,4.5-0.8c1.6,0,2.9,0,4.4,0.8
	c0.7,0.4,1.9,1,2.5,2.5c0.7,1.8-0.1,3.4-0.3,3.9c-0.7,1.4-1.8,2.2-2.2,2.4c-1.4,0.9-2.7,0.9-4.7,1c-1.6,0-2.6,0.1-3.4-0.6
	c-0.6-0.4-1.4-1.4-1.2-2.3c0.3-1.1,1.8-1.3,2-1.3c1.1-0.2,2,0.3,2.7,0.7c0.5,0.3,1,0.7,1.8,1.4c0.7,0.6,1.2,1.1,1.5,1.4"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/re.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#re">RE</button></p>
    </div>
    <div class="modal fade" id="re" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA RE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">

                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">
</style>
                        <path class="stm" d="M45.2,44c1.1-0.5,2.5-1.2,4.3-1.9c1.6-0.7,1.8-0.7,2-0.6c1,0.9-7.2,10.9-6.6,11.5c0.8,0.7,14.2-13.6,15.4-12.6
		c0.3,0.2-0.3,0.9-0.9,3.5c-0.6,2.3-0.7,4-0.8,5.6c-0.2,2.7-0.3,4.1,0.4,5.2c0.2,0.3,1.2,2.1,3.2,2.2c2,0.1,3.2-1.5,3.3-1.7"/>
                        <path class="stl" d="M49.4,37.9c0,6.5,0,13,0,19.6"/>
                        <path class="stl" d="M113.8,56"/>

	</svg>


                </div>
            </div>
        </div>
    </div>

@endsection
@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ro.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ro">RO</button></p>
    </div>
    <div class="modal fade" id="ro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA RO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">

                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">
</style>

                        <path class="stm" d="M45.9,39.8c1.2,0,3.1-0.1,5.5-0.4c2.5-0.2,3.1-0.4,3.3-0.1c1,1.5-11.9,11.5-11.5,12c0.2,0.3,3-2.2,7.5-3
		c2-0.4,4.3-0.8,6,0.6c1.9,1.6,1.6,4.4,1.6,4.6c-0.2,2.2-1.7,3.6-2.4,4.1c-1.6,1.5-3.5,1.8-4.6,2c-1.3,0.2-2.5,0.2-3.3,0.1"/>

		</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
