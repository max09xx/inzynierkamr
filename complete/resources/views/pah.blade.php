@extends('layouts.app')

@section ('column')
<hr>
<table class="table table-striped">

    <tr>
        <th>

        </th>
        <th>
            <p>A</p>
        </th>
        <th>
            <p>I</p>
        </th>
        <th>
            <p>U</p>
        </th>
        <th>
            <p>E</p>
        </th>
        <th>
            <p>O</p>
        </th>
    </tr>
    <tr>
        <th>
            <p>A</p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#a">A</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#i">I</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#u">U</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#e">E</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#o">O</button></p>
        </th>
    </tr>

    <tr>
        <th>
            <p>K</p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ka">
                    KA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ki">
                    KI</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ku">
                    KU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ke">
                    KE</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ko">
                    KO</button></p>
        </th>
    </tr>
    <tr>
        <th>
            <p>S</p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sa">
                    SA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#shi">
                    SHI</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#su">
                    SU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#se">
                    SE</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#so">
                    SO</button></p>
        </th>
    </tr>
    <tr>
        <th>
            <p>T</p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ta">
                    TA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#chi">
                    CHI</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tsu">
                    TSU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#te">
                    TE</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#to">
                    TO</button></p>
        </th>
    </tr>
    <tr>
        <th>
            <p>N</p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#na">
                    NA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ni">
                    NI</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#nu">
                    NU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ne">
                    NE</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#no">
                    NO</button></p>
        </th>
    </tr>
    <tr>
        <th>
            <p>H</p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ha">
                    HA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#hi">
                    HI</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fu">
                    FU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#he">
                    HE</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ho">
                    HO</button></p>
        </th>
    </tr>
    <tr>
        <th>
            <p>M</p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ma">
                    MA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mi">
                    MI</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mu">
                    MU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#me">
                    ME</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mo">
                    MO</button></p>
        </th>
    </tr>
    <tr>
        <th>
            <p>Y</p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ya">
                    YA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#yi">
                    I</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#yu">
                    YU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ye">
                    E</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#yo">
                    YO</button></p>
        </th>
    </tr>
    <tr>
        <th>
            <p>R</p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ra">
                    RA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ri">
                    RI</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ru">
                    RU</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#re">
                    RE</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ro">
                    RO</button></p>
        </th>
    </tr>
    <tr>
        <th>
            <p>W</p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#wa">
                    WA</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#wi">
                    I</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#wu">
                    U</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#we">
                    E</button></p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#wo">
                    O</button></p>
        </th>
    </tr>
    <tr>
        <th>
            <p>N</p>
        </th>
        <th>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#nn">
                    N</button></p>
        </th>
    </tr>
</table>

<!--A-->

<div class="modal fade" id="a" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA A</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">
</style>
                    <path class="stm" d="M43.1,45.1c2.1,0.2,3.9,0.1,5.3,0c0.5,0,2-0.2,3.9-0.6c1.2-0.2,2.2-0.5,3-0.8"/>

                    <path class="sts" d="M49.2,39.2c0,2-0.1,3.7-0.2,4.8c-0.1,1.5-0.1,1.9-0.3,4.4c-0.2,2.8-0.2,3.5-0.2,4.1c0,0.5,0,1.3,0.2,3.4
	c0.1,1.3,0.3,3.2,0.5,5.4"/>

                    <path class="std" d="M53.9,48.3c-0.4,1.1-1,2.3-1.6,3.6c-0.8,1.6-1.5,2.7-2.1,3.5c-1.2,1.8-1.8,2.7-2.7,3.6
	c-0.9,0.8-1.7,1.5-2.9,1.8c-0.5,0.1-2,0.4-3-0.6c-0.9-0.9-0.7-2.3-0.6-2.6c0.2-0.9,0.7-1.5,1.6-2.4c1.1-1.2,2.2-1.9,2.8-2.2
	c1.8-1.1,3.3-1.5,4.4-1.8c1.6-0.4,2.9-0.8,4.6-0.6c0.9,0.1,1.9,0.2,3.1,0.9c0.5,0.3,1.5,0.9,2.1,2.3c0.6,1.2,0.5,2.4,0.4,3
	c-0.1,0.5-0.3,2-1.4,3.3c-0.8,0.9-1.8,1.3-2.3,1.6c-0.7,0.3-1.3,0.4-1.7,0.5"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="i" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA I</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="sts" d="M42.5,45.7c-0.1,2.9,0.3,5.3,0.6,7c0.4,2.2,1,4,2.4,5.3c0.3,0.3,1.1,1.1,1.6,0.8c0.5-0.2,0.5-1.2,0.3-2.6"/>
                    <path class="stm" d="M56.7,45.8c1,1.1,2.1,2.7,3.1,4.8c0.8,1.6,1.3,3.2,1.6,4.4"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="u" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA U</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M49.3,41.1c0.6,0.2,1.5,0.5,2.3,1.1c0.9,0.6,1.6,1.2,2.1,1.7"/>
                    <path class="sts" d="M46.3,50.2c0.5-0.5,1.4-1.4,2.8-2c0.8-0.3,2.2-1,4-0.6c0.4,0.1,2.1,0.5,3.2,1.9c1.3,1.8,0.7,4,0.3,5.6
	c-0.4,1.7-1.1,2.8-2.1,4.4c-0.8,1.2-1.9,3-3.7,4.8"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="e" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA E</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M47.3,42.8c1.7,0.9,3.4,1.9,5.1,2.8"/>
                    <path class="stdd" d="M43.6,50.9c5.6-2.3,9.8-3.5,10.5-2.4c1.5,2.4-12.6,15-12.8,14.8c-0.2-0.2,6.5-7.4,8.7-6.9
	c3.3,0.6,0.5,5.1,1.7,6.8c0.6,0.9,2.1,1.1,3.3,1c2.4-0.3,5.6-1.5,4.4-1"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="o" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA O</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M56.5,45.6c0.6,0.3,1.4,0.8,2.3,1.5c0.7,0.6,1.3,1.2,1.7,1.8"/>
                    <path class="sts" d="M42,49.7c1.4-0.1,3.1-0.3,5.1-0.9c1.6-0.5,2.9-1,4-1.6"/>
                    <path class="std" d="M46.9,43.3C46.7,46.5,47,49.2,47,51c-0.1,3.3,0.5,4.4,0.2,8c-0.2,2.5-0.5,3.6-1.3,4c-1.1,0.6-3.4,0.6-3.9-0.3
	c-0.8-1.5,1.1-4.2,1.4-4.5c1.3-1.3,3.6-2.2,6.2-2.9c2.9-0.8,4.8-1.1,5.3-1.1c2.4-0.1,3.7,0.5,4.7,1.5c1.4,1.5,0.6,4,0.5,4.3
	c-1,3.1-4.6,3.9-4.8,3.9"/>
</svg>
            </div>
        </div>
    </div>
</div>

<!--K-->

<div class="modal fade" id="ka" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA KA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M57,42.8c0.7,0.3,2.1,1,3.3,2.4c1,1.2,1.5,2.4,1.8,3.2"/>
                    <path class="sts" d="M49.2,38.3c-2.1,5.8-4.1,11.6-6.2,17.4"/>
                    <path class="std" d="M40.9,46.1c5.3,0.3,7.4-0.8,9.4-0.9c0.6,0,1-0.2,1.6-0.2c1.9,0.2,0.8,3.3,0.5,4.5c-0.4,1.9-1.1,4.8-3.3,8"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ki" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA KI</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M49.6,37.6c2.2,4.9,4.4,9.8,6.7,14.7"/>
                    <path class="sts" d="M45.1,44.8c3.9-1.3,7.9-2.7,11.8-4"/>
                    <path class="std" d="M47.7,49.6c3.6-1.2,7.2-2.3,10.8-3.5"/>
                    <path class="stl" d="M46,55.2c0.6,0.8,1.9,2,3.9,2.9c2.5,1.1,4.7,1,5.8,0.8"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ku" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA KU</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M54.1,38c-0.9,0.9-2.4,2.3-4.2,4c-3.9,3.7-4.8,4.3-4.8,5.6c0,1.4,1.3,2.1,4.7,5.3c2,1.9,3.5,3.5,4.6,4.7"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ke" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA KE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M43.2,38.2c-0.4,1.9-0.8,4.8-0.9,8.2c0,1.2-0.2,7.2,0.9,7.4c0.3,0,0.7-0.3,1.4-1.8"/>
                    <path class="sts" d="M56,37.5c0.2,2.8,0.3,5.7,0.4,8.8c0.1,2.5,0.1,4.2-0.6,6.2c-0.6,1.7-1.4,2.9-2,3.8"/>
                    <path class="std" d="M49.4,43.7c1.5,0.1,3.3,0.2,5.4-0.1c2.4-0.3,4.4-0.9,5.9-1.4"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ko" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA KO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M44.8,40.8c3.5-0.5,6.2-0.3,8.1,0c0.9,0.1,2.6,0.4,2.7,0.8c0,0.4-1.4,0.8-2.3,1"/>
                    <path class="sts" d="M43.2,54.4c0.9,0.9,2.6,2.2,5.2,3c2.9,0.9,5.2,0.6,5.7,0.5c1.7-0.3,3-0.8,3.8-1.3"/>
</svg>
            </div>
        </div>
    </div>
</div>

<!--S-->

<div class="modal fade" id="sa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA SA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M48.4,40.3c2.5,4.2,5,8.4,7.5,12.6"/>
                    <path class="sts" d="M44.3,48.5c1.9-0.4,4.1-0.9,6.5-1.8c2.5-0.9,4.6-1.8,6.4-2.8"/>
                    <path class="std" d="M45.5,56.6c0.5,0.7,1.8,2.1,3.9,2.9c2.6,1,4.8,0.6,5.6,0.4"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="shi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA SHI</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M46.8,36.5c0.1,5.4,0.1,9.7,0,13.2c0,1-0.1,2.8,1,4.3c1.4,2,3.9,2.4,4.3,2.5c3.7,0.6,6.5-2,6.8-2.3"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="su" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA SU</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M41.8,45.8c3.2,0,5.8-0.2,7.8-0.4c2.9-0.3,5.2-0.7,7-1.1c1.8-0.3,3.3-0.7,4.4-0.9"/>
                    <path class="sts" d="M52.8,39.8c0.3,3.3,0.5,5.9,0.8,7.6c0.4,3.2,0.7,5-0.4,6.5c-0.2,0.3-1.1,1.5-2.5,1.5c-1.1,0-2.1-0.7-2.6-1.8
	c-0.6-1.4,0.2-2.6,0.3-2.9c0.6-1,2-1.9,3.2-1.5c0.8,0.3,1.3,1,1.5,1.5c0.9,1.6,0.5,3.4,0.1,5.3c-0.2,1.1-0.4,1.9-1,2.9
	c-0.6,1.1-1.4,1.8-1.9,2.2"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="se" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA SE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="sts" d="M47.5,41.4c0,3.2,0,5.8-0.1,7.5c-0.1,3.7-0.3,4.9,0.7,6c1,1.1,2.5,1.3,5.3,1.6c2.3,0.2,4.2,0.1,5.5,0"/>
                    <path class="stm" d="M56.7,40.6c0,1.9-0.1,3.9-0.3,6c-0.1,1.7-0.3,3.4-0.4,5.1"/>
                    <path class="std" d="M42.6,48.3c3.4-0.4,6.1-0.8,8-1.1c1.1-0.2,1.7-0.3,4.4-0.8c2.7-0.5,5-0.8,6.4-1.1"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="so" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA SO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M47.8,40.7c2.7-0.1,5.2-0.5,7.3-1.1c0.9-0.3,1.6-0.5,1.7-0.3c0.3,0.6-5.2,4.7-10.4,8c-1.5,0.9-2.7,1.6-2.6,1.9
	c0.2,0.4,2.5-0.4,9-1.8c0,0,1.9-0.4,5.2-0.9c0,0,2.9-0.5,2.9-0.4c0,0.1-3.2,0-6.2,1.9c-2.3,1.4-3.1,3.1-3.3,3.6
	c-0.3,0.7-0.9,1.9-0.5,3.3c0.3,1.2,1.2,1.9,2.1,2.6c0.8,0.7,2.5,1.7,4.9,2.2"/>
</svg>
            </div>
        </div>
    </div>
</div>

<!--T-->


<div class="modal fade" id="ta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA TA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M49.8,39.8c-2.6,6.9-5.2,13.7-7.8,20.6"/>
                    <path class="sts" d="M42.6,45.5c3.6-0.3,7.3-0.7,10.9-1"/>
                    <path class="std" d="M52.9,50.7c0.6-0.3,1.3-0.7,2.3-0.9c1.9-0.5,3.5-0.3,4.4-0.2"/>
                    <path class="stl" d="M52.3,57.5c0.5,0.5,1.2,1,2.1,1.5c2.4,1.2,4.6,0.8,5.5,0.6"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="chi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA CHI</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M41.9,43.4c2.5-0.3,5.3-0.7,8.2-1.4c1.3-0.3,2.6-0.6,3.8-1"/>
                    <path class="sts" d="M48.1,37.5c-0.3,4.6-1.2,8.3-2,10.8c-0.5,1.6-1.3,3.9-0.8,4.2c0.5,0.3,1.7-2.2,4.7-3.3c0.9-0.3,3.5-1.1,5.8,0.3
	c0.3,0.2,2.5,1.6,2.4,3.7c0,1.7-1.5,2.8-2.5,3.6c-2.3,1.8-5,2-6.3,2"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="tsu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA TSU</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M40.7,45.6c4.1-1,6.3-1.6,8.1-2c2.8-0.7,4.4-1.1,6.7-0.9c1.7,0.1,3.9,0.3,5,1.9c0.9,1.3,0.6,2.9,0.4,3.8
	c-0.1,0.7-0.5,2.5-2.1,4c-2.5,2.5-6,2.1-6.4,2"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="te" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA TE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M42,42.8c5.1-0.8,9.2-1.5,12.2-2c0.7-0.1,4.2-0.8,4.3-0.4c0.1,0.3-2.1,0.6-4.3,2.4c-0.3,0.3-2,1.6-3.3,4
	c-0.7,1.3-1.3,2.6-1.3,4.3c0,2.1,1,3.6,1.3,4.2c1.6,2.4,3.9,3.2,4.7,3.5"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="to" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA TO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M46,39.4c1.5,2.6,3,5.3,4.5,7.9"/>
                    <path class="sts" d="M58.8,42.9c-4.2,2.1-7.4,4.2-9.6,5.8c-1.6,1.2-3,2.3-3.3,4.1c-0.2,1.4,0.2,3.1,1.3,4.1c0.8,0.7,1.7,1,4.9,1.2
	c1.7,0.1,3.9,0.2,6.6,0.2"/>
</svg>
            </div>
        </div>
    </div>
</div>

<!--N-->


<div class="modal fade" id="na" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA NA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M48.6,39.4c-1.8,4.6-3.6,9.2-5.4,13.8"/>
                    <path class="sts" d="M41.7,45c3.1-0.5,6.2-1.1,9.3-1.6"/>
                    <path class="std" d="M56.4,44.1c1.2,1,2.4,1.9,3.7,2.9"/>
                    <path class="stl" d="M54.8,49.4c0.3,2.8,0.2,5-0.1,6.5c-0.3,2.1-0.5,3.3-1.6,4.1c-1.4,1-4.1,1-5-0.6c-0.5-0.9-0.4-2.2,0.3-3
	c0.9-1.1,2.6-1,3.9-0.9c2.8,0.2,5,1.4,6.6,2.6"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ni" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA NI</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M43.3,39.8c-0.3,3.6-0.5,6.2-0.7,8c-0.2,2.2-0.4,4.1,0.3,6.4c0.3,1,0.7,1.8,1,2.3"/>
                    <path class="sts" d="M51.1,43.4c1.1-0.5,2.5-1,4.3-1.3c1.7-0.3,3.2-0.4,4.3-0.3"/>
                    <path class="std" d="M50.7,52.3c0.4,0.6,1.8,2.2,4.3,3c2.5,0.8,4.6,0.2,5.3-0.1"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="nu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA NU</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M46,41.3c0,1.9,0.1,4.4,0.8,7.3c0.8,3.2,2,5.6,3,7.3"/>
                    <path class="sts" d="M53.7,39.6c-0.4,3.1-1.1,5.6-1.8,7.3c-0.2,0.7-0.9,2.4-2,4.6c-1.9,3.6-3.2,4.2-3.8,4.4
	c-0.2,0.1-1.9,0.7-2.8-0.2c-1-0.8-0.7-2.6-0.6-3.1c0.3-1.5,1.2-2.5,2.3-3.6c0.8-0.8,2.3-2.4,4.9-3.5c1-0.4,3-1.3,5.8-1.3
	c1.9,0,3.6,0.1,5.2,1.3c1.7,1.4,2.4,3.7,2.2,5.7c-0.2,1.6-1,2.7-1.8,3.6c-0.9,1.2-1.9,2.5-3.6,2.7c-1.1,0.1-2.8-0.2-3.3-1.5
	c-0.4-1,0-2.2,0.8-2.8c1.1-0.8,2.6-0.2,3.8,0.3c1.1,0.4,3.2,1.5,5.2,4.1"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ne" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA NE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M46.7,40.1c-0.1,6.5-0.2,13.1-0.3,19.6"/>
                    <path class="sts" d="M42.3,46.1c2.3-0.3,3.9-1,5-1.5c0.5-0.2,0.7-0.4,0.9-0.3c0.6,0.5-2.3,5.7-5.8,9.8c-0.4,0.5-1.1,1.3-1.1,1.4
	c0.2,0.1,5.7-5,8.1-7.1c1.5-1.3,2.8-2.5,4.9-3.2c1-0.3,3-0.9,4.4,0.1c1,0.7,1.1,2,1.3,4c0.2,2,0,3.5-0.3,4.8
	c-0.4,2.2-0.6,3.3-1.4,4.1c-0.1,0.1-1.7,1.6-3.4,1.1C54,59,53,58.2,53,57.1c0-1.1,1.3-1.8,2.1-2.1c1.1-0.3,2.1,0.1,2.9,0.5
	c2.1,1,3.3,2.7,3.9,3.8"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="no" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA NO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M52,43c-0.5,2.3-1,4.1-1.5,5.5c-0.5,1.4-1.4,3.2-3.3,6.6c-0.4,0.8-0.9,1.6-1.7,1.8c-1,0.1-1.9-1.1-2.3-1.6
	c-1.6-2.1-1.4-4.7-1.3-4.9c0.3-2.2,1.6-3.6,2.4-4.6c0.7-0.8,2.2-2.1,4.4-2.9c3.9-1.4,7.3,0,8.2,0.4c1.3,0.6,3,1.3,4.2,3.3
	c0.2,0.4,1.4,2.4,0.8,4.9c-0.4,1.6-1.3,2.6-2.3,3.7c-0.9,1-2.8,2.6-5.7,3.7"/>
</svg>
            </div>
        </div>
    </div>
</div>

<!--H-->


<div class="modal fade" id="ha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA HA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M41.5,39c-0.4,2.4-0.7,5.2-0.6,8.4c0,3.6,0.5,6.7,1,9.3"/>
                    <path class="sts" d="M55.8,38.5c-0.3,5.1,0,8.9,0.3,11.6c0.2,1.9,0.5,3.9-0.8,5.5c-1.3,1.6-3.4,1.8-4.1,1.9
	c-0.9,0.1-2.1,0.1-2.9-0.8c-0.5-0.6-0.8-1.6-0.4-2.3c0.4-0.7,1.4-0.8,3-0.9c0.9-0.1,2-0.1,3.3,0.1c3.1,0.6,5.1,2.7,5.9,3.5"/>
                    <path class="std" d="M49.4,44c3.3-0.5,6.6-1,9.9-1.5"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="hi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA HI</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M42.5,42c2.7-0.2,4.8-0.8,6.3-1.4c0.9-0.3,1.8-0.8,2-0.5c0.3,0.4-1.4,1.7-3,4.1c-0.5,0.7-1.4,2.3-2.1,4.4
	c-0.8,2.5-1.6,5-0.4,7c1.2,1.9,3.4,2.3,3.9,2.4c3.3,0.5,5.8-2,6.3-2.5c1.3-1.4,1.7-3,2.5-6.3c0.4-1.5,0.7-3,0.8-5
	c0-2.1-0.4-3.2-0.1-3.4c0.3-0.1,1.4,1.1,5,7.6"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="fu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA FU</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stl" d="M38.6,54.4c0.1,0.6,0.2,1.5,0.8,2.5c0.5,1,1.2,1.6,1.6,2"/>
                    <path class="sts" d="M47.6,41.1c0.5,0.1,1.4,0.4,2.3,1c0.8,0.6,1.2,1.2,1.5,1.6"/>
                    <path class="std" d="M57.5,52.1c0.7,0.7,1.5,1.6,2.3,2.9c0.5,0.9,0.9,1.8,1.1,2.5"/>
                    <path class="stm" d="M47.8,49.3c0.4,0.1,2.3,0.5,3.6,2.4c1.3,1.8,1.1,3.7,1,4.4c-0.1,0.7-0.3,1.7-1.1,2.6c-1.3,1.4-3.3,1.3-3.6,1.3"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="he" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA HE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M42.3,50.9c1-0.6,2.4-1.4,3.6-2.8c1.5-1.6,2.1-3,3.1-3c0.7,0,1,0.6,2.1,1.9c0,0,1.7,1.9,4.1,3.9
	c1.4,1.1,2.5,1.8,3.9,2.7c1.6,1,3,1.7,4.1,2.3"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ho" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA HO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M43.8,42.3c-0.4,2.2-0.6,4-0.7,5.3c-0.2,2.6-0.3,5,0.2,8c0.3,1.9,0.8,3.3,1.2,4.3"/>
                    <path class="sts" d="M51.1,42.9c3.5-0.4,7-0.9,10.5-1.3"/>
                    <path class="std" d="M51.3,49c3.6-0.6,7.2-1.1,10.8-1.7"/>
                    <path class="stl" d="M57.9,41.8c-0.5,5.4-0.2,9.3,0.2,12c0.2,1.2,0.6,3.4-0.7,5.3c-1.1,1.6-2.9,2-3.1,2c-0.5,0.1-2.1,0.4-3.3-0.7
	c-0.6-0.6-1.2-1.7-0.8-2.4c0.4-0.9,2.1-0.9,3.3-0.9c2.8,0,5,1.2,6,1.8c1.3,0.7,2.3,1.5,3,2.2"/>
</svg>
            </div>
        </div>
    </div>
</div>

<!--M-->

<div class="modal fade" id="ma" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA MA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="std" d="M51.8,39.2c-0.2,6.7-0.1,10.6,0.2,13c0,0.2,0.1,1.8-0.3,4.3c-0.2,1.5-0.6,2.7-1.7,3.5c-1.4,1.1-3.2,0.7-3.5,0.7
	c-0.6-0.1-2-0.4-2.4-1.5c-0.2-0.6,0-1.3,0.3-1.8c0.9-1.2,3-1,4.2-0.8c0.7,0.1,1.9,0.3,4.6,1.5c1.2,0.5,2.8,1.3,4.6,2.5"/>
                    <path class="sts" d="M44.6,44.4c4-0.5,8-1,12-1.5"/>
                    <path class="stm" d="M45.4,50.3c3.8-0.4,7.6-0.9,11.3-1.3"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="mi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA MI</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M47.4,39.8c2.3,0,4-0.3,5.2-0.7c0.9-0.3,1.5-0.5,1.8-0.3c0.5,0.5-1.1,2.6-2.8,5.5c-1.2,2-1.2,2.5-3.2,6.3
	c0,0-0.8,1.5-2.4,3.5c-1.2,1.5-1.7,1.8-2.3,1.8c-0.9,0.1-1.8-0.5-2.2-1.3c-0.6-1.4,0.7-2.8,0.8-3c0.3-0.3,1-0.9,3.3-1.3
	c1.8-0.3,3.1-0.2,5.3,0c3.2,0.2,4.8,0.4,5.1,0.4c1.6,0.3,4,0.9,6.8,2.5"/>
                    <path class="sts" d="M59.3,47.3c0,1.3-0.2,3.6-1.3,6.1c-1.1,2.4-2.6,4.1-3.5,5"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="mu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA MU</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M47.8,41.1c-0.7,5.6-0.2,8.8-0.2,10.7c0,1.2,0.3,1.6-0.1,3.4c-0.3,1.2-0.6,2.8-1.8,3.3c-0.9,0.4-2.2,0-2.9-0.8
	c-0.9-1-0.5-2.3-0.4-2.8c0-0.1,0.6-2.4,2.3-2.7c0.7-0.1,1.5,0.2,2.1,0.8c0.9,0.9,0.4,2,0.9,5.5c0.2,1.6,0.7,1.9,0.9,2.2
	c0.7,0.7,1.4,0.8,3.4,0.8c2.2,0,3.4,0,4.8-0.8c0.6-0.3,1.8-0.9,2.3-2.2c0.1-0.2,0.2-0.6,0.3-2c0.1-0.8,0.1-2,0-3.4"/>
                    <path class="sts" d="M42.8,46.4c3-0.4,6.1-0.9,9.1-1.3"/>
                    <path class="std" d="M57.6,43.5c0.6,0.3,1.5,0.9,2.3,1.8c0.6,0.7,1,1.3,1.3,1.8"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="me" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA ME</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M45.4,42.2c0.1,2.1,0.3,4.8,1.2,7.8c0.7,2.6,1.6,4.8,2.5,6.5"/>
                    <path class="sts" d="M53.4,40.8c-0.4,3-1.2,5.3-1.8,6.8c0,0-0.9,1.6-2.6,4.8c-0.5,1-1.3,2.3-2.6,3.8c-1.5,1.6-2,1.4-2.2,1.3
	c-1.1-0.2-1.8-1.7-1.9-2.2c-0.1-0.4-0.6-1.9,0.2-3.4c0.4-0.8,0.8-1.2,2.5-2.3c2.6-1.8,3.8-2.7,4.8-3.1c0.6-0.3,2.5-1,5-1.2
	c1.3-0.1,2.3-0.2,3.7,0.3c0.6,0.2,2.2,0.7,3.5,2.3c0.4,0.5,1.7,2.1,1.7,4.3c-0.1,1.6-0.8,2.8-1.2,3.3c-0.9,1.4-2,2.1-2.6,2.4
	c-1.3,0.8-2.5,1.1-3.3,1.2"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="mo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA MO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M44.6,44.4c2.6,0.7,5.2,1.5,7.8,2.2"/>
                    <path class="sts" d="M49.8,40.4c-0.3,1.8-0.8,4.2-1.3,7.1c-0.8,4.8-1.1,6.6-0.6,9c0.3,1.8,0.7,3.7,2.4,4.8c2,1.2,4.3,0.4,4.6,0.3
	c2.1-0.8,3-2.7,3.3-3.3c1.1-2.4,0.4-4.6,0.3-5.1"/>
                    <path class="std" d="M43.6,51.8c2.8,0.5,5.6,1,8.4,1.5"/>
</svg>
            </div>
        </div>
    </div>
</div>

<!--Y-->

<div class="modal fade" id="ya" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA YA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M42.1,41c2.3,6.5,4.6,12.9,6.9,19.4"/>
                    <path class="sts" d="M39.1,48.3c3.3-1.4,6.1-2.5,8.3-3.3c0.7-0.3,2.3-0.8,4.4-1.3c1.9-0.4,3.1-0.6,4.6-0.3c0.8,0.2,3.2,0.8,3.6,2.5
	c0.4,1.4-0.8,2.7-1.1,3.1c-1.3,1.5-3,1.7-3.5,1.8"/>
                    <path class="std" d="M48.4,38.6c0.9,0.9,1.8,1.8,2.8,2.6"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="yi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA I</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="sts" d="M42.5,45.7c-0.1,2.9,0.3,5.3,0.6,7c0.4,2.2,1,4,2.4,5.3c0.3,0.3,1.1,1.1,1.6,0.8c0.5-0.2,0.5-1.2,0.3-2.6"/>
                    <path class="stm" d="M56.7,45.8c1,1.1,2.1,2.7,3.1,4.8c0.8,1.6,1.3,3.2,1.6,4.4"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="yu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA YU</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M42.1,44.5c0.1,1.5,0.2,3.7,0.3,6.4c0,0,0.1,3.5,0.3,3.5c0.1,0-0.7-3.3,1-5.9c0.8-1.2,1.8-1.8,4-3.1
	c2.3-1.4,3.6-2.1,5.5-2.4c1.1-0.1,3.6-0.5,5.8,1.1c1.8,1.3,2.4,3,2.5,3.5c0.3,0.8,0.7,3-0.5,5.1c-1,1.7-2.6,2.4-3.3,2.6
	c-1.7,0.7-3.3,0.5-4.6,0.4c-1.6-0.2-2.9-0.6-3.8-1"/>
                    <path class="sts" d="M52.9,38.3c0.6,2.5,0.9,4.7,1,6.3c0.2,2.5,0,2.1,0.3,6.5c0.1,1.6,0.2,2.4,0,3.8c-0.1,1.1-0.3,2.5-1.3,3.9
	c-0.7,1.1-1.6,1.7-2.1,2.1"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ye" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA E</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M47.3,42.8c1.7,0.9,3.4,1.9,5.1,2.8"/>
                    <path class="stdd" d="M43.6,50.9c5.6-2.3,9.8-3.5,10.5-2.4c1.5,2.4-12.6,15-12.8,14.8c-0.2-0.2,6.5-7.4,8.7-6.9
	c3.3,0.6,0.5,5.1,1.7,6.8c0.6,0.9,2.1,1.1,3.3,1c2.4-0.3,5.6-1.5,4.4-1"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="yo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA YO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M51.8,39.2c0.1,3.3,0.1,6.3,0.2,8.7c0.1,9.6-0.2,3.5-0.1,5.6c0.1,2.2,0.2,3.2-0.5,4.3c-0.8,1.1-2,1.5-3,1.8
	c-0.8,0.2-3.9,1.1-5-0.5c-0.6-0.8-0.5-2.2,0.1-3c0.9-1.2,2.5-1.2,3.9-1.1c1.2,0,3,0.3,6.9,2.1c1.3,0.6,3.2,1.6,5.3,2.9"/>

                    <path class="std" d="M58.5,43.4c-2.1,0.5-4.3,0.9-6.4,1.4"/>
</svg>
            </div>
        </div>
    </div>
</div>

<!--R-->

<div class="modal fade" id="ra" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA RA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M46.8,45c0.4,3.1,0.2,5.5,0,7.1c-0.1,1-0.4,2.3,0,2.5c0.4,0.2,0.9-0.7,2.1-1.7c1.9-1.5,3.8-1.9,4.4-2
	c0.9-0.2,2.1-0.4,3.6,0c0.6,0.2,2.1,0.6,3,2c0.2,0.3,0.9,1.4,0.7,2.8c-0.2,1.1-0.9,1.8-1.4,2.3c-0.8,0.7-1.6,1.1-2.3,1.3
	c-1.8,0.7-3.5,1.4-5,1.9"/>
                    <path class="sts" d="M48.2,38.3c1.1,1.1,2.2,2.1,3.3,3.2"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA RI</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M46,44c-0.5,1.9-0.7,3.5-0.8,4.6c-0.1,2.2,0.3,3.3,0.4,3.6c0.3,0.9,0.8,1.7,1.1,2.1"/>
                    <path class="sts" d="M54.1,43.7c0.7,2,1,3.8,1,5.1c0,0.6,0.4,1.2,0,4.7c-0.3,3.1-0.9,4.4-1,4.9c-0.4,1.4-1.2,3.4-3.4,5.5"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ru" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA RU</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M46.8,41.7c2.6,0,4.6-0.3,6-0.7c1.3-0.3,2.6-0.8,2.8-0.5c0.1,0.2-0.8,0.8-1.8,1.7c-1.4,1.3-1.4,1.8-3,3.8
	c-1.4,1.7-2.7,3-4.6,4.7c-1.3,1.2-2.5,2.2-2.4,2.3c0.1,0.1,1.4-1.3,3.7-2.3c1.9-0.8,3.6-0.8,4.5-0.8c1.6,0,2.9,0,4.4,0.8
	c0.7,0.4,1.9,1,2.5,2.5c0.7,1.8-0.1,3.4-0.3,3.9c-0.7,1.4-1.8,2.2-2.2,2.4c-1.4,0.9-2.7,0.9-4.7,1c-1.6,0-2.6,0.1-3.4-0.6
	c-0.6-0.4-1.4-1.4-1.2-2.3c0.3-1.1,1.8-1.3,2-1.3c1.1-0.2,2,0.3,2.7,0.7c0.5,0.3,1,0.7,1.8,1.4c0.7,0.6,1.2,1.1,1.5,1.4"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="re" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA RE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">

                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">
</style>
                    <path class="stm" d="M45.2,44c1.1-0.5,2.5-1.2,4.3-1.9c1.6-0.7,1.8-0.7,2-0.6c1,0.9-7.2,10.9-6.6,11.5c0.8,0.7,14.2-13.6,15.4-12.6
		c0.3,0.2-0.3,0.9-0.9,3.5c-0.6,2.3-0.7,4-0.8,5.6c-0.2,2.7-0.3,4.1,0.4,5.2c0.2,0.3,1.2,2.1,3.2,2.2c2,0.1,3.2-1.5,3.3-1.7"/>
                    <path class="sts" d="M49.4,37.9c0,6.5,0,13,0,19.6"/>
                    <path class="std" d="M113.8,56"/>

	</svg>


            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA RO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">

                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">
</style>

                    <path class="stm" d="M45.9,39.8c1.2,0,3.1-0.1,5.5-0.4c2.5-0.2,3.1-0.4,3.3-0.1c1,1.5-11.9,11.5-11.5,12c0.2,0.3,3-2.2,7.5-3
		c2-0.4,4.3-0.8,6,0.6c1.9,1.6,1.6,4.4,1.6,4.6c-0.2,2.2-1.7,3.6-2.4,4.1c-1.6,1.5-3.5,1.8-4.6,2c-1.3,0.2-2.5,0.2-3.3,0.1"/>

		</svg>
            </div>
        </div>
    </div>
</div>

<!--W-->

<div class="modal fade" id="wa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA WA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">
</style>
                    <path class="stm" d="M42.9,45.6c0.9-0.2,2.3-0.6,3.9-1.3c1.4-0.6,1.8-0.9,2-0.8c0.8,0.8-7,10.3-6.4,10.9c0.3,0.2,1.7-1.1,7.1-5.3
		c2.5-2,3.7-2.8,5.6-3.4c1.5-0.4,3.7-1.1,5.6,0.1c2.3,1.4,2.5,4.3,2.5,4.5c0.1,2-1,3.5-1.6,4.4c-0.9,1.2-1.9,1.9-3,2.6
		c-1.5,1-2.9,1.6-3.9,2"/>
                    <path class="sts" d="M47.5,39.3c0,6.5-0.1,13.1-0.1,19.6"/>
	</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="wi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA I</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="sts" d="M42.5,45.7c-0.1,2.9,0.3,5.3,0.6,7c0.4,2.2,1,4,2.4,5.3c0.3,0.3,1.1,1.1,1.6,0.8c0.5-0.2,0.5-1.2,0.3-2.6"/>
                    <path class="stm" d="M56.7,45.8c1,1.1,2.1,2.7,3.1,4.8c0.8,1.6,1.3,3.2,1.6,4.4"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="wu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA U</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M49.3,41.1c0.6,0.2,1.5,0.5,2.3,1.1c0.9,0.6,1.6,1.2,2.1,1.7"/>
                    <path class="sts" d="M46.3,50.2c0.5-0.5,1.4-1.4,2.8-2c0.8-0.3,2.2-1,4-0.6c0.4,0.1,2.1,0.5,3.2,1.9c1.3,1.8,0.7,4,0.3,5.6
	c-0.4,1.7-1.1,2.8-2.1,4.4c-0.8,1.2-1.9,3-3.7,4.8"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="we" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA E</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                    <path class="stm" d="M47.3,42.8c1.7,0.9,3.4,1.9,5.1,2.8"/>
                    <path class="stdd" d="M43.6,50.9c5.6-2.3,9.8-3.5,10.5-2.4c1.5,2.4-12.6,15-12.8,14.8c-0.2-0.2,6.5-7.4,8.7-6.9
	c3.3,0.6,0.5,5.1,1.7,6.8c0.6,0.9,2.1,1.1,3.3,1c2.4-0.3,5.6-1.5,4.4-1"/>
</svg>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="wo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA O</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">

                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>

                    <path class="stm" d="M51.6,36c-6.1,9-7.4,11.4-7.1,11.6c0.3,0.2,2.2-2.6,4.6-2.3c1.7,0.2,2.7,1.8,2.9,2.1c1.5,2.5,0,5.2-0.1,5.4"/>
                    <path class="sts" d="M57.4,46.4c-4.2,1.1-7,2.9-8.5,4.1c-2.8,2.2-2.7,3.4-2.6,3.8c0.2,1.3,1.8,2.1,2.6,2.5c2.6,1.3,5.3,0.8,6.6,0.5"/>
                    <path class="std" d="M43.5,40.3c4-0.3,8-0.5,12-0.8"/>

		</svg>
            </div>
        </div>
    </div>
</div>

<!--N-->

<div class="modal fade" id="nn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">HIRAGANA SOLO N</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">
</style>
                    <path class="stm" d="M54,38.3C48.2,49.4,43.7,58,43.7,58c0,0,4.5-9.6,7.2-9c0.6,0.1,1.6,0.9,1.8,1.8c0.1,0.5,0,0.7,0,1.6
		c0,0.7-0.1,2,0.2,2.9c0.6,1.6,2.7,2.2,3.3,2.3c2.9,0.8,5.3-0.8,5.5-1c1.9-1.3,2.5-3.2,2.7-3.8"/>
		</svg>
            </div>
        </div>
    </div>
</div>

<hr>
@endsection
