@extends('layouts.lekcje3')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kya.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ya">YA</button></p>
    </div>
    <div class="modal fade" id="ya" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA YA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M43.2,34.2c3.7,10.4,7.4,20.8,11.2,31.2"/>
                        <path class="std" d="M35.2,47.8c15.1-3.8,25.7-6.5,27.9-7.3c0.2-0.1,1.1-0.4,1.5-0.1c0.8,0.6,0.1,2.9-0.1,3.7
		C64,45.7,62.9,48.3,60,51"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kyu.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#yu">YU</button></p>
    </div>
    <div class="modal fade" id="yu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA YU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M42.5,39.9c8.7,0,15,0,16.1,0c0.2,0,0.8,0,1.1,0.4c0.3,0.3,0.3,0.8,0.3,1c-0.2,2.8-0.4,12.9-0.4,14.7
		c0,0.5,0,1.4,0,2.5"/>
                        <path class="std" d="M36.9,58.3c11.3,0,22.5,0,33.8-0.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kyo.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#yo">YO</button></p>
    </div>
    <div class="modal fade" id="yo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA YO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M38.3,40.7c7.3,0.1,14.5,0.1,21.8,0.2"/>
                        <path class="sts" d="M60,40.4c-0.1,9-0.2,17.9-0.3,26.9"/>
                        <path class="std" d="M39.2,51.8c6.9,0,13.8-0.1,20.7-0.1"/>
                        <path class="stl" d="M38.3,63.8c7.2-0.1,14.3-0.1,21.5-0.2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
