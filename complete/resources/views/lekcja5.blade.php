@extends('layouts.lekcje')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/na.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#na">NA</button></p>
    </div>
    <div class="modal fade" id="na" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA NA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M48.6,39.4c-1.8,4.6-3.6,9.2-5.4,13.8"/>
                        <path class="sts" d="M41.7,45c3.1-0.5,6.2-1.1,9.3-1.6"/>
                        <path class="std" d="M56.4,44.1c1.2,1,2.4,1.9,3.7,2.9"/>
                        <path class="stl" d="M54.8,49.4c0.3,2.8,0.2,5-0.1,6.5c-0.3,2.1-0.5,3.3-1.6,4.1c-1.4,1-4.1,1-5-0.6c-0.5-0.9-0.4-2.2,0.3-3
	c0.9-1.1,2.6-1,3.9-0.9c2.8,0.2,5,1.4,6.6,2.6"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ni.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ni">NI</button></p>
    </div>
    <div class="modal fade" id="ni" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA NI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M43.3,39.8c-0.3,3.6-0.5,6.2-0.7,8c-0.2,2.2-0.4,4.1,0.3,6.4c0.3,1,0.7,1.8,1,2.3"/>
                        <path class="sts" d="M51.1,43.4c1.1-0.5,2.5-1,4.3-1.3c1.7-0.3,3.2-0.4,4.3-0.3"/>
                        <path class="std" d="M50.7,52.3c0.4,0.6,1.8,2.2,4.3,3c2.5,0.8,4.6,0.2,5.3-0.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/nu.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#nu">NU</button></p>
    </div>
    <div class="modal fade" id="nu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA NU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M46,41.3c0,1.9,0.1,4.4,0.8,7.3c0.8,3.2,2,5.6,3,7.3"/>
                        <path class="sts" d="M53.7,39.6c-0.4,3.1-1.1,5.6-1.8,7.3c-0.2,0.7-0.9,2.4-2,4.6c-1.9,3.6-3.2,4.2-3.8,4.4
	c-0.2,0.1-1.9,0.7-2.8-0.2c-1-0.8-0.7-2.6-0.6-3.1c0.3-1.5,1.2-2.5,2.3-3.6c0.8-0.8,2.3-2.4,4.9-3.5c1-0.4,3-1.3,5.8-1.3
	c1.9,0,3.6,0.1,5.2,1.3c1.7,1.4,2.4,3.7,2.2,5.7c-0.2,1.6-1,2.7-1.8,3.6c-0.9,1.2-1.9,2.5-3.6,2.7c-1.1,0.1-2.8-0.2-3.3-1.5
	c-0.4-1,0-2.2,0.8-2.8c1.1-0.8,2.6-0.2,3.8,0.3c1.1,0.4,3.2,1.5,5.2,4.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ne.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ne">NE</button></p>
    </div>
    <div class="modal fade" id="ne" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA NE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M46.7,40.1c-0.1,6.5-0.2,13.1-0.3,19.6"/>
                        <path class="sts" d="M42.3,46.1c2.3-0.3,3.9-1,5-1.5c0.5-0.2,0.7-0.4,0.9-0.3c0.6,0.5-2.3,5.7-5.8,9.8c-0.4,0.5-1.1,1.3-1.1,1.4
	c0.2,0.1,5.7-5,8.1-7.1c1.5-1.3,2.8-2.5,4.9-3.2c1-0.3,3-0.9,4.4,0.1c1,0.7,1.1,2,1.3,4c0.2,2,0,3.5-0.3,4.8
	c-0.4,2.2-0.6,3.3-1.4,4.1c-0.1,0.1-1.7,1.6-3.4,1.1C54,59,53,58.2,53,57.1c0-1.1,1.3-1.8,2.1-2.1c1.1-0.3,2.1,0.1,2.9,0.5
	c2.1,1,3.3,2.7,3.9,3.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/no.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#no">NO</button></p>
    </div>
    <div class="modal fade" id="no" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA NO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M52,43c-0.5,2.3-1,4.1-1.5,5.5c-0.5,1.4-1.4,3.2-3.3,6.6c-0.4,0.8-0.9,1.6-1.7,1.8c-1,0.1-1.9-1.1-2.3-1.6
	c-1.6-2.1-1.4-4.7-1.3-4.9c0.3-2.2,1.6-3.6,2.4-4.6c0.7-0.8,2.2-2.1,4.4-2.9c3.9-1.4,7.3,0,8.2,0.4c1.3,0.6,3,1.3,4.2,3.3
	c0.2,0.4,1.4,2.4,0.8,4.9c-0.4,1.6-1.3,2.6-2.3,3.7c-0.9,1-2.8,2.6-5.7,3.7"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
