@extends('layouts.lekcje')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kha.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ha">HA</button></p>
    </div>
    <div class="modal fade" id="ha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA HA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M44,43.3c-0.6,2.5-1.6,5.4-3.2,8.7c-1.7,3.5-3.5,6.2-5.2,8.3"/>
                        <path class="sts" d="M57.2,41.3c1.6,2.1,3.4,4.7,5,7.7c2.3,4.2,3.8,8.2,4.8,11.5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/khi.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#hi">HI</button></p>
    </div>
    <div class="modal fade" id="hi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA HI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M43.3,39.8c-0.1,13.4-0.2,23.2-0.3,24.3c0,0.2,0,0.8,0.3,1.3c0,0,0.3,0.5,0.9,0.9c0.9,0.6,9.2,0.7,20.6,0.3"/>
                        <path class="stl" d="M43.5,51c3.5-0.4,7.2-1,11.3-1.8c3.3-0.7,6.4-1.6,9.3-2.4"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kfu.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fu">FU</button></p>
    </div>
    <div class="modal fade" id="fu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA FU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M38.8,41.1c13.4,0,22.4-0.1,23.9-0.3c0.2,0,0.9-0.1,1.4,0.3c0.5,0.5,0.3,1.4,0.3,1.8c-1,5.6-0.6,5.1-1.1,6.5
		c-0.2,0.5-1,2.5-2.6,4.7c-1.4,1.9-2.8,3.1-4.2,4.3c-0.9,0.8-2.7,2.2-5.2,3.6c-1.8,1-4.4,2.3-7.7,3.2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/khe.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#he">HE</button></p>
    </div>
    <div class="modal fade" id="he" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA HE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M38.6,57c5.7-5.5,8.5-8.6,9.7-10.5c0.3-0.4,1.1-1.6,2.1-1.6c1.1,0,2,1.2,2.3,1.6c1.4,1.9,6.2,6.4,16.3,15.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kho.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ho">HO</button></p>
    </div>
    <div class="modal fade" id="ho" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA HO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M35.8,45.9c10.6,0,21.2,0,31.8,0"/>
                        <path class="std" d="M51.3,35.9c0,11.1,0.1,22.1,0.1,33.2"/>
                        <path class="stl" d="M43.1,52.8c-2.1,3.6-4.2,7.1-6.3,10.7"/>
                        <path class="stdd" d="M59.3,52.8c2.8,3.5,5.7,6.9,8.5,10.4"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
