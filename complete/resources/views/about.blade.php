
@extends('layouts.app')

@section('column')
    <div class="cir">
        <img class="rounded-circle img-fluid" src="/img/ja2.jpg" width=300px height=300px alt="Coś poszło nie tak"/>
    </div>
@endsection

@section('columnle')

    <div class="gif">
        <h3>Student Politechniki Opolskiej</h3>

        <p>Skończona szkoła: Zespół Szkół nr 3 im. Macieja Rataja w Tomaszowie Lubelskim</p>

        <p>Kierunek: Informatyka</p>

        <p>Tytuł po szkole średniej: Technik Informatyk</p>
    </div>

@endsection

@section('columnri')

    <div class="gif">
        <h3>Hobby</h3>

        <p>Anime</p>

        <p>Manga</p>

        <p>Kultura Japonii</p>

        <p>Seriale</p>
    </div>

@endsection

