@extends('layouts.lekcje2')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kwa.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#wa">WA</button></p>
    </div>
    <div class="modal fade" id="wa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA WA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M40,38L40,38c-0.1,3.6-0.1,7.2-0.2,10.8"/>
                        <path class="sts" d="M40,38.4c12.2-0.3,21.2-0.5,23.1-0.5c0.4,0,1.3,0,1.8,0.6c0.6,0.7,0.3,1.8,0.2,2.3c-2.3,9-2,8.6-2.7,10
		c-1.2,2.4-2.6,5.4-5.7,8.1c-1.8,1.5-4.1,3-5.3,3.8c-1.3,0.8-2.4,1.4-3.3,1.9"/>
</svg>
                </div>
            </div>
        </div>
    </div>

    @endsection

@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kwo.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#wo">WO</button></p>
    </div>
    <div class="modal fade" id="wo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA WO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<path class="stm" d="M37.8,40.1c11.6-0.2,20.1-0.3,22.7-0.5c0.6,0,2.2-0.2,3.1,0.8c0.5,0.5,0.6,1.2,0.7,1.8
		c0.3,4.1-2.7,9.3-2.7,9.3c-2.8,4.8-4.2,7.3-6.4,9.3c-4.3,4-9.1,5.5-11.8,6.1"/>
                        <path class="stl" d="M39.2,49.2c7.9-0.1,15.8-0.2,23.6-0.2"/>
	</svg>
                </div>
            </div>
        </div>
    </div>

    @endsection
