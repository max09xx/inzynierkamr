<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>KJJ</title>
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
@include('inc.navbar')

<div id="app" class="gif">
    <div class="container">
        <div class="row">
            <div class="col-12 mt-5">
                <h3>
                    Twoim zadaniem jest ułożenie danych liter w odpowiedniej kolejności tak aby stworzyły wyświetlone słowo.
                </h3>
            </div>
        </div>


        <div class="row">
            <div class="col-12 mt-5">
                @yield('slowo')
            </div>
        </div>


        <div class="row">
            <div class="col-12 col-md-3">

            </div>
            <div class="col-12 col-md-6 mt-5">
                @yield('znaki')
            </div>
            <div class="col-12 col-md-3">

            </div>
        </div>


        <div class="row">
            <div class="col-12 col-md-3">

            </div>
            <div class="col-12 col-md-6 mt-5">
                @yield('odpowiedz')
            </div>
            <div class="col-12 col-md-3">

            </div>
        </div>
    </div>
</div>

<footer id="footer" class="text-center">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">

            </div>
            <div class="col-12 col-md-6">
                <p>Copyright 2020 &copy; Maksymilian Rebizant</p>
            </div>
            <div class="col-12 col-md-3">
                <div class="right">
                    <a href="https://www.facebook.com/Collegium2016/"> <img src="img/fb.png" width=40px height=40px alt="Coś poszło nie tak"></a>
                    <a href="https://www.instagram.com/murzynekfreezz/?hl=pl"> <img src="img/ig.png" width=40px height=40px alt="Coś poszło nie tak"></a>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="/js/app.js"></script>
</body>
</html>
