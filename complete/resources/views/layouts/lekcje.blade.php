<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>KJJ</title>
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
@include('inc.navbar')

<div class="table">
    <table class="table table-striped">
        <tbody>
        <tr>
            <td>@yield('table1')</td>
        </tr>
        <tr>
            <td>@yield('table2')</td>
        </tr>
        <tr>
            <td>@yield('table3')</td>
        </tr>
        <tr>
            <td>@yield('table4')</td>
        </tr>
        <tr>
            <td>@yield('table5')</td>
        </tr>
        <tr>
            <td>@yield('table6')</td>
        </tr>
        <tr>
            <td>@yield('table7')</td>
        </tr>
        </tbody>
    </table>
</div>

<footer id="footer" class="text-center">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">

            </div>
            <div class="col-12 col-md-6">
                <p>Copyright 2020 &copy; Maksymilian Rebizant</p>
            </div>
            <div class="col-12 col-md-3">
                <div class="right">
                    <a href="https://www.facebook.com/Collegium2016/"> <img src="img/fb.png" width=40px height=40px alt="Coś poszło nie tak"></a>
                    <a href="https://www.instagram.com/murzynekfreezz/?hl=pl"> <img src="img/ig.png" width=40px height=40px alt="Coś poszło nie tak"></a>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
