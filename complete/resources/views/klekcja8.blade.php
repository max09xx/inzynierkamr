@extends('layouts.lekcje')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kta.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ta">TA</button></p>
    </div>
    <div class="modal fade" id="ta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA TA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M48.2,36.8c-0.7,2.4-1.9,5.5-4,8.8c-1.8,3-3.8,5.2-5.5,6.8"/>
                        <path class="sts" d="M45.8,42.7c6.8,0.1,11.7,0.1,13.3,0.1c0.1,0,0.3,0,0.5,0.1c1.5,0.8-0.2,6.3-0.3,6.6c-0.7,2.1-1.5,3.5-3.1,6.1
		c-1.5,2.5-2.4,3.9-3.9,5.6c-2,2.1-3.8,3.4-5.4,4.5c-1.8,1.2-3.5,2.1-4.8,2.8"/>
                        <path class="stl" d="M47,51c4.5,2.8,8.9,5.6,13.4,8.4"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kchi.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#chi">CHI</button></p>
    </div>
    <div class="modal fade" id="chi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA CHI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M62,40.8c-2.8,0.9-6.4,1.8-10.7,2.3c-4.2,0.5-7.9,0.4-10.8,0.2"/>
                        <path class="sts" d="M36.2,52c10.4-0.1,20.8-0.1,31.2-0.2"/>
                        <path class="std" d="M53.4,43.2c-0.2,4.4-0.1,8,0,10.4c0.1,2.9,0.3,4.3-0.5,6.3c-0.9,2.1-2.4,3.6-4.1,5.3c-1.6,1.5-3.1,2.6-4.3,3.4
		"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/ktsu.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tsu">TSU</button></p>
    </div>
    <div class="modal fade" id="tsu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA TSU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M40.7,42.3c1.4,2.4,2.9,4.8,4.3,7.2"/>
                        <path class="sts" d="M50.5,39.7c1.3,2.4,2.6,4.9,3.8,7.3"/>
                        <path class="std" d="M67.2,40.3c0.1,2.8-0.1,7.5-2.5,12.5c-0.8,1.7-2.1,4.4-5,7c-2,1.8-4,2.8-6,3.8c-2.4,1.2-4.6,2-6.2,2.5"/>

</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kte.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#te">TE</button></p>
    </div>
    <div class="modal fade" id="te" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA TE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M40.9,39.8c6.7,0,13.4-0.1,20.2-0.1"/>
                        <path class="sts" d="M35.3,49.8c10.6-0.1,21.2-0.1,31.8-0.2"/>
                        <path class="std" d="M52.6,49.7c0.1,1,0.1,2.3,0.1,3.8c0,0.9-0.1,2.5-0.5,4.3c-0.2,1.1-0.6,2.9-1.6,4.8c-0.6,1.3-2,3.5-4.5,5.5"/>

</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kto.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#to">TO</button></p>
    </div>
    <div class="modal fade" id="to" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA TO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M47.1,34.9C47.1,45,47,55.1,47,65.3"/>
                        <path class="std" d="M47,47.7c2.2,0.1,5.4,0.5,9,1.9c2.8,1.1,4.9,2.4,6.5,3.6"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
