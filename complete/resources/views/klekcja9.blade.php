@extends('layouts.lekcje')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/ksa.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sa">SA</button></p>
    </div>
    <div class="modal fade" id="sa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA SA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M33.8,48c11.8,0,23.5,0.1,35.3,0.1"/>
                        <path class="std" d="M43.8,38.4c0,6.3,0.1,12.7,0.1,19"/>
                        <path class="stl" d="M58.1,37.2C58,38.7,58,40.7,58,43c0,1.7,0.1,2.2,0.1,5.1c0.1,2.9,0.1,4.4,0,5.9c-0.2,2.7-0.2,4-0.8,5.3
		c-0.8,1.9-2,2.9-4.3,4.8c-1.4,1.1-3.5,2.7-6.3,4.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kshi.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#shi">SHI</button></p>
    </div>
    <div class="modal fade" id="shi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA SHI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M43.7,39.3c2.4,1.4,4.8,2.9,7.2,4.3"/>
                        <path class="sts" d="M38.8,48.5c2.4,1.6,4.8,3.1,7.2,4.7"/>
                        <path class="std" d="M42.7,67.8c3.2-1.1,5.8-2.3,7.7-3.2c3.3-1.6,5.5-2.6,8.2-4.7c2.4-1.9,4.1-3.7,5.5-5.2c1.7-1.9,3-3.5,4-4.8"/>

</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/ksu.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#su">SU</button></p>
    </div>
    <div class="modal fade" id="su" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA SU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M42.4,40.6c9.2-0.2,16-0.4,17.8-0.3c0.4,0,1.4,0,1.8,0.6c0.3,0.5,0.1,1.1,0,1.5c-1.2,3.6-4.4,6.9-4.4,6.9
		c-2,2.1-3,3.1-4.9,4.9c-1.9,1.8-3.6,3.4-6,5.3c-1.7,1.3-3.9,3-6.6,4.8"/>
                        <path class="std" d="M55.2,51.8c2.4,1.5,5.1,3.5,7.8,6c2.3,2.1,4.3,4.2,5.9,6.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kse.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#se">SE</button></p>
    </div>
    <div class="modal fade" id="se" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA SE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M35.1,51.8c14.1-2.6,24.2-4.4,26.5-5c0.4-0.1,1.9-0.5,2.2,0.1c0.2,0.4-0.2,1-0.3,1.3c-1.5,2.5-6.1,8.8-6.8,9.7
		"/>
                        <path class="std" d="M46.1,38.3c0.2,14.2,0.3,24.4,0.2,26.1c0,0.3-0.1,1.3,0.5,1.8c0.5,0.4,1.1,0.3,1.3,0.3
		c1.5-0.1,7.6-0.2,16.2-0.3"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyKatakana/kso.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#so">SO</button></p>
    </div>
    <div class="modal fade" id="so" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KATAKANA SO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
	<path class="stm" d="M36.1,39.8c1.8,2.8,3.5,5.5,5.3,8.3"/>
                        <path class="sts" d="M59.8,38.6c0.1,2.6,0,7.3-2.4,12.4c-1.8,3.9-4.1,6.3-5.6,7.9c-3.7,3.8-7.6,6.1-10.6,7.5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
