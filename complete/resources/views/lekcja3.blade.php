@extends('layouts.lekcje')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/sa.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sa">SA</button></p>
    </div>
    <div class="modal fade" id="sa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA SA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M48.4,40.3c2.5,4.2,5,8.4,7.5,12.6"/>
                        <path class="sts" d="M44.3,48.5c1.9-0.4,4.1-0.9,6.5-1.8c2.5-0.9,4.6-1.8,6.4-2.8"/>
                        <path class="std" d="M45.5,56.6c0.5,0.7,1.8,2.1,3.9,2.9c2.6,1,4.8,0.6,5.6,0.4"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/shi.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#shi">SHI</button></p>
    </div>
    <div class="modal fade" id="shi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA SHI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M46.8,36.5c0.1,5.4,0.1,9.7,0,13.2c0,1-0.1,2.8,1,4.3c1.4,2,3.9,2.4,4.3,2.5c3.7,0.6,6.5-2,6.8-2.3"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/su.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#su">SU</button></p>
    </div>
    <div class="modal fade" id="su" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA SU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M41.8,45.8c3.2,0,5.8-0.2,7.8-0.4c2.9-0.3,5.2-0.7,7-1.1c1.8-0.3,3.3-0.7,4.4-0.9"/>
                        <path class="sts" d="M52.8,39.8c0.3,3.3,0.5,5.9,0.8,7.6c0.4,3.2,0.7,5-0.4,6.5c-0.2,0.3-1.1,1.5-2.5,1.5c-1.1,0-2.1-0.7-2.6-1.8
	c-0.6-1.4,0.2-2.6,0.3-2.9c0.6-1,2-1.9,3.2-1.5c0.8,0.3,1.3,1,1.5,1.5c0.9,1.6,0.5,3.4,0.1,5.3c-0.2,1.1-0.4,1.9-1,2.9
	c-0.6,1.1-1.4,1.8-1.9,2.2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/se.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#se">SE</button></p>
    </div>
    <div class="modal fade" id="se" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA SE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="sts" d="M47.5,41.4c0,3.2,0,5.8-0.1,7.5c-0.1,3.7-0.3,4.9,0.7,6c1,1.1,2.5,1.3,5.3,1.6c2.3,0.2,4.2,0.1,5.5,0"/>
                        <path class="stm" d="M56.7,40.6c0,1.9-0.1,3.9-0.3,6c-0.1,1.7-0.3,3.4-0.4,5.1"/>
                        <path class="std" d="M42.6,48.3c3.4-0.4,6.1-0.8,8-1.1c1.1-0.2,1.7-0.3,4.4-0.8c2.7-0.5,5-0.8,6.4-1.1"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/so.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#so">SO</button></p>
    </div>
    <div class="modal fade" id="so" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA SO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M47.8,40.7c2.7-0.1,5.2-0.5,7.3-1.1c0.9-0.3,1.6-0.5,1.7-0.3c0.3,0.6-5.2,4.7-10.4,8c-1.5,0.9-2.7,1.6-2.6,1.9
	c0.2,0.4,2.5-0.4,9-1.8c0,0,1.9-0.4,5.2-0.9c0,0,2.9-0.5,2.9-0.4c0,0.1-3.2,0-6.2,1.9c-2.3,1.4-3.1,3.1-3.3,3.6
	c-0.3,0.7-0.9,1.9-0.5,3.3c0.3,1.2,1.2,1.9,2.1,2.6c0.8,0.7,2.5,1.7,4.9,2.2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
