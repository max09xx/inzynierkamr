@extends('layouts.lekcje')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ha.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ha">HA</button></p>
    </div>
    <div class="modal fade" id="ha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA HA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M41.5,39c-0.4,2.4-0.7,5.2-0.6,8.4c0,3.6,0.5,6.7,1,9.3"/>
                        <path class="sts" d="M55.8,38.5c-0.3,5.1,0,8.9,0.3,11.6c0.2,1.9,0.5,3.9-0.8,5.5c-1.3,1.6-3.4,1.8-4.1,1.9
	c-0.9,0.1-2.1,0.1-2.9-0.8c-0.5-0.6-0.8-1.6-0.4-2.3c0.4-0.7,1.4-0.8,3-0.9c0.9-0.1,2-0.1,3.3,0.1c3.1,0.6,5.1,2.7,5.9,3.5"/>
                        <path class="std" d="M49.4,44c3.3-0.5,6.6-1,9.9-1.5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/hi.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#hi">HI</button></p>
    </div>
    <div class="modal fade" id="hi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA HI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M42.5,42c2.7-0.2,4.8-0.8,6.3-1.4c0.9-0.3,1.8-0.8,2-0.5c0.3,0.4-1.4,1.7-3,4.1c-0.5,0.7-1.4,2.3-2.1,4.4
	c-0.8,2.5-1.6,5-0.4,7c1.2,1.9,3.4,2.3,3.9,2.4c3.3,0.5,5.8-2,6.3-2.5c1.3-1.4,1.7-3,2.5-6.3c0.4-1.5,0.7-3,0.8-5
	c0-2.1-0.4-3.2-0.1-3.4c0.3-0.1,1.4,1.1,5,7.6"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/fu.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fu">FU</button></p>
    </div>
    <div class="modal fade" id="fu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA FU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stl" d="M38.6,54.4c0.1,0.6,0.2,1.5,0.8,2.5c0.5,1,1.2,1.6,1.6,2"/>
                        <path class="sts" d="M47.6,41.1c0.5,0.1,1.4,0.4,2.3,1c0.8,0.6,1.2,1.2,1.5,1.6"/>
                        <path class="std" d="M57.5,52.1c0.7,0.7,1.5,1.6,2.3,2.9c0.5,0.9,0.9,1.8,1.1,2.5"/>
                        <path class="stm" d="M47.8,49.3c0.4,0.1,2.3,0.5,3.6,2.4c1.3,1.8,1.1,3.7,1,4.4c-0.1,0.7-0.3,1.7-1.1,2.6c-1.3,1.4-3.3,1.3-3.6,1.3"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/he.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#he">HE</button></p>
    </div>
    <div class="modal fade" id="he" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA HE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M42.3,50.9c1-0.6,2.4-1.4,3.6-2.8c1.5-1.6,2.1-3,3.1-3c0.7,0,1,0.6,2.1,1.9c0,0,1.7,1.9,4.1,3.9
	c1.4,1.1,2.5,1.8,3.9,2.7c1.6,1,3,1.7,4.1,2.3"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ho.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ho">HO</button></p>
    </div>
    <div class="modal fade" id="ho" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA HO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M43.8,42.3c-0.4,2.2-0.6,4-0.7,5.3c-0.2,2.6-0.3,5,0.2,8c0.3,1.9,0.8,3.3,1.2,4.3"/>
                        <path class="sts" d="M51.1,42.9c3.5-0.4,7-0.9,10.5-1.3"/>
                        <path class="std" d="M51.3,49c3.6-0.6,7.2-1.1,10.8-1.7"/>
                        <path class="stl" d="M57.9,41.8c-0.5,5.4-0.2,9.3,0.2,12c0.2,1.2,0.6,3.4-0.7,5.3c-1.1,1.6-2.9,2-3.1,2c-0.5,0.1-2.1,0.4-3.3-0.7
	c-0.6-0.6-1.2-1.7-0.8-2.4c0.4-0.9,2.1-0.9,3.3-0.9c2.8,0,5,1.2,6,1.8c1.3,0.7,2.3,1.5,3,2.2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
