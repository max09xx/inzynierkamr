@extends('layouts.lekcje')

@section('table1')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/ma.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ma">MA</button></p>
    </div>
    <div class="modal fade" id="ma" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA MA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="std" d="M51.8,39.2c-0.2,6.7-0.1,10.6,0.2,13c0,0.2,0.1,1.8-0.3,4.3c-0.2,1.5-0.6,2.7-1.7,3.5c-1.4,1.1-3.2,0.7-3.5,0.7
	c-0.6-0.1-2-0.4-2.4-1.5c-0.2-0.6,0-1.3,0.3-1.8c0.9-1.2,3-1,4.2-0.8c0.7,0.1,1.9,0.3,4.6,1.5c1.2,0.5,2.8,1.3,4.6,2.5"/>
                        <path class="sts" d="M44.6,44.4c4-0.5,8-1,12-1.5"/>
                        <path class="stm" d="M45.4,50.3c3.8-0.4,7.6-0.9,11.3-1.3"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table2')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/mi.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mi">MI</button></p>
    </div>
    <div class="modal fade" id="mi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA MI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M47.4,39.8c2.3,0,4-0.3,5.2-0.7c0.9-0.3,1.5-0.5,1.8-0.3c0.5,0.5-1.1,2.6-2.8,5.5c-1.2,2-1.2,2.5-3.2,6.3
	c0,0-0.8,1.5-2.4,3.5c-1.2,1.5-1.7,1.8-2.3,1.8c-0.9,0.1-1.8-0.5-2.2-1.3c-0.6-1.4,0.7-2.8,0.8-3c0.3-0.3,1-0.9,3.3-1.3
	c1.8-0.3,3.1-0.2,5.3,0c3.2,0.2,4.8,0.4,5.1,0.4c1.6,0.3,4,0.9,6.8,2.5"/>
                        <path class="std" d="M59.3,47.3c0,1.3-0.2,3.6-1.3,6.1c-1.1,2.4-2.6,4.1-3.5,5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table3')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/mu.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mu">MU</button></p>
    </div>
    <div class="modal fade" id="mu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA MU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M47.8,41.1c-0.7,5.6-0.2,8.8-0.2,10.7c0,1.2,0.3,1.6-0.1,3.4c-0.3,1.2-0.6,2.8-1.8,3.3c-0.9,0.4-2.2,0-2.9-0.8
	c-0.9-1-0.5-2.3-0.4-2.8c0-0.1,0.6-2.4,2.3-2.7c0.7-0.1,1.5,0.2,2.1,0.8c0.9,0.9,0.4,2,0.9,5.5c0.2,1.6,0.7,1.9,0.9,2.2
	c0.7,0.7,1.4,0.8,3.4,0.8c2.2,0,3.4,0,4.8-0.8c0.6-0.3,1.8-0.9,2.3-2.2c0.1-0.2,0.2-0.6,0.3-2c0.1-0.8,0.1-2,0-3.4"/>
                        <path class="std" d="M42.8,46.4c3-0.4,6.1-0.9,9.1-1.3"/>
                        <path class="stl" d="M57.6,43.5c0.6,0.3,1.5,0.9,2.3,1.8c0.6,0.7,1,1.3,1.3,1.8"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table4')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/me.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#me">ME</button></p>
    </div>
    <div class="modal fade" id="me" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA ME</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M45.4,42.2c0.1,2.1,0.3,4.8,1.2,7.8c0.7,2.6,1.6,4.8,2.5,6.5"/>
                        <path class="sts" d="M53.4,40.8c-0.4,3-1.2,5.3-1.8,6.8c0,0-0.9,1.6-2.6,4.8c-0.5,1-1.3,2.3-2.6,3.8c-1.5,1.6-2,1.4-2.2,1.3
	c-1.1-0.2-1.8-1.7-1.9-2.2c-0.1-0.4-0.6-1.9,0.2-3.4c0.4-0.8,0.8-1.2,2.5-2.3c2.6-1.8,3.8-2.7,4.8-3.1c0.6-0.3,2.5-1,5-1.2
	c1.3-0.1,2.3-0.2,3.7,0.3c0.6,0.2,2.2,0.7,3.5,2.3c0.4,0.5,1.7,2.1,1.7,4.3c-0.1,1.6-0.8,2.8-1.2,3.3c-0.9,1.4-2,2.1-2.6,2.4
	c-1.3,0.8-2.5,1.1-3.3,1.2"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('table5')

    <div class="gif">
        <figure class="figure">
            <img src="/img/screenyHiragana/mo.JPG" class="figure-img img-fluid rounded" alt="Tu powinien być gif litery">
        </figure>
    </div>

    <div class="gif">
        <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mo">MO</button></p>
    </div>
    <div class="modal fade" id="mo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HIRAGANA MO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <svg version="1.1" id="litera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
<style type="text/css">

</style>
                        <path class="stm" d="M44.6,44.4c2.6,0.7,5.2,1.5,7.8,2.2"/>
                        <path class="sts" d="M49.8,40.4c-0.3,1.8-0.8,4.2-1.3,7.1c-0.8,4.8-1.1,6.6-0.6,9c0.3,1.8,0.7,3.7,2.4,4.8c2,1.2,4.3,0.4,4.6,0.3
	c2.1-0.8,3-2.7,3.3-3.3c1.1-2.4,0.4-4.6,0.3-5.1"/>
                        <path class="std" d="M43.6,51.8c2.8,0.5,5.6,1,8.4,1.5"/>
</svg>
                </div>
            </div>
        </div>
    </div>

@endsection
